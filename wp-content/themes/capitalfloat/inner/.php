       <?php /* Template Name: about */ ?>
<?php get_header('comparision'); ?>


<section class="w-100 business-financing mb-5">

          <div class="container pt-5">

            <h3 class="col-md-10 offset-md-1 col-lg-6 offset-lg-3 text-center light-blue font-3 px-5 px-sm-0 my-1 my-sm-5 font-weight-600">Our Values</h3>

            <div class="row mb-5">

              <div class="col-sm-6 px-sm-2 px-lg-5">

                <div class="ico-bus-fin1 pl-3 pl-sm-0">

                  <img class="pl-3 pl-sm-0" src="assets/images/icons/vector-smart-object-2.png" alt="easy sign up" width="auto" height="50">

                </div>

                <div class="sub-content1 pl-0 pls-sm-2">

                  <h5>Quality</h5>

                  <p class="pr-5 pr-sm-3 pr-lg-5 font-15">We use technology to deliver outstanding products and top-level service to help small businesses grow.</p>

              </div>

              </div>

              <div class="col-sm-6 px-sm-2 px-lg-5">

                  <div class="ico-bus-fin1 pl-3 pl-sm-0">

                <img class="pl-3 pl-sm-0" src="assets/images/icons/vector-smart-object-1.png" alt="assesssment" width="auto" height="50">

              </div>

              <div class="sub-content1 pl-0 pls-sm-2">

                <h5 >Innovation</h5>

                <p class="pr-5 pr-sm-3 pr-lg-5 font-15">We bridge the traditional boundaries of finance and technology to meet our customers’ needs.</p>

              </div>

              </div>

              <div class="col-sm-6 px-sm-2 px-lg-5 mt-0 mt-sm-5">

                  <div class="ico-bus-fin1 pl-3 pl-sm-0">

                <img class="pl-3 pl-sm-0" src="assets/images/icons/vector-smart-object21.png" alt="Get Funds" width="auto" height="50">

              </div>

              <div class="sub-content1 pl-0 pls-sm-2">

                <h5>Integrity</h5>

                <p class="pr-5 pr-sm-3 pr-lg-5 font-15">We undertake all our actions with the highest standards of integrity.</p>

              </div>

            </div>

              <div class="col-sm-6 px-sm-2 px-lg-5 mt-0 mt-sm-5">

                  <div class="ico-bus-fin1 pl-3 pl-sm-0">

                <img class="pl-3 pl-sm-0" src="assets/images/icons/vector-smart-object-about.png" alt="Get Funds" width="auto" height="50">

              </div>

              <div class="sub-content1 pl-0 pl-sm-2">

                <h5>Commitment</h5>

                <p class="pr-5 pr-sm-3 pr-lg-5 font-15">We develop strong relationships with all our customers. Every member of the team is personally accountable for delivering on our commitments.</p>

              </div>

            </div>

            </div>

          </div>

        </section>

        <section class="w-100 access-to-more mb-5">

            <div class="container pt-5">

                <div class="row mb-5">
                
                    <div class="col-md-4 col-12 mt-5">

                            <h3 class="col-md-10 offset-md-1 col-lg-6 offset-lg-3 text-center light-blue font-3 d-sm-inline">Our Team</h3>
            
                            <p class="px-2 px-sm-2 px-lg-2 font-14 text-center mt-3">Finaxar is a dedicated and growing team of seasoned veterans from banking, supply chain management, data science, and technology, with proven track records and a passion for solving real problems.</p>            

                            <p class="px-2 px-sm-2 px-lg-2 font-14 text-center mt-3">Our technology-driven processes are thoughtfully designed by people who understand the challenges of small business financing.</p>            
                            
                    </div>
                    <div class="col-md-8 col-12 mt-5 res-scroll-container">
                         
                    <div class="row">
                        <div class="col-4 mt-2">

                            <div class="pl-3 pl-sm-0 text-center">
            
                                <img class="pl-sm-0 rounded-circle blueBorder" src="assets/images/member1.png" alt="easy sign up" width="auto" height="60">
            
                            </div>
            
                            <div class="mb-0 mt-1 my-sm-3 pl-0 pls-sm-2">
            
                                <h5 class="text-center">Deepak</h5>
                                <p class="text-center">MANAGER</p>
                                <p class="font-14 text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            
                            </div>
            
                            </div>
            
                            <div class="col-4 mt-2">
            
                                <div class="pl-3 pl-sm-0 text-center">
            
                            <img class="pl-sm-0 rounded-circle blueBorder" src="assets/images/member2.png" alt="assesssment" width="auto" height="60">
            
                            </div>
            
                            <div class="mb-0 mt-1 my-sm-3 pl-0 pls-sm-2">
            
                            <h5 class="text-center">Neeraj Singh</h5>
    
                            <p class="text-center">CO-FOUNDER/CEO</p>
            
                            <p class="font-14 text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            
                            </div>
            
                            </div>
            
                            <div class="col-4 mt-2">
            
                                <div class="pl-3 pl-sm-0 text-center">
            
                            <img class="pl-sm-0 rounded-circle blueBorder" src="assets/images/member3.png" alt="Get Funds" width="auto" height="60">
            
                            </div>
            
                            <div class="mb-0 mt-1 my-sm-3 pl-0 pls-sm-2">
            
                            <h5 class="text-center">Priya Gandhi</h5>
    
                            <p class="text-center">DIRECTOR</p>
                                
                            <p class="font-14 text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            
                            </div>
            
                            </div>
                        </div>
                    </div>
                    

                </div>

                <div class="row justify-content-center pb-5">
                    <div class="col-2 text-right">
                        <img src="assets/images/monk.png" alt="" width="auto" height="50">
                    </div>
                    <div class="col-2"></div>
                    <div class="col-2 text-center">
                        <img src="assets/images/fiveHundred.png" alt="" width="auto" height="50">
                    </div>
                    <div class="col-9 pt-5">
                        <p class="font-14 text-center">We are backed by Monk’s Hill Ventures, 500 Startups, other funds and family offices from Singapore, USA, and Europe. We finance using our own funds and are not a marketplace lender.</p>
                    </div>
                </div>

            </div>

        </section>


        <section class="w-100 applie d-flex h-100">
            <div class="backgroundCont">
                <img src="assets/images/bg-blue.png" alt="" class="web-bg-abt">
            </div>
            <div class="container align-self-center">
                <div class="row justify-content-center">
                    <div class="col-md-5 applyCol text-md-left text-center">
                        <h2 class="text-light">Keen to give it a try?</h2>
                        <p class="text-light pt-3">Apply for Funds Today!</p>
                    </div>
                    <div class="col-md-3 offset-md-1 text-md-left text-center applyCol">
                        <a class="col-md-10 col-6 mx-md-0 mx-auto light-blue text-uppercase btn btn-outline-light rounded-pill btn-lg btn-block font-14" href="#">Apply Now</a>
                        <p class="text-light mt-3"><span class="mr-2"><img src="assets/images/icons/snap-icon.png" alt=""> </span>Only takes 2 minutes!</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="w-100 bottomLine mb-5">
            <div class="container borderr">
                <div class="col-sm-6 col-md-7 mx-auto">
                    
                    <h5 class="text-center font-weight-500 black1 px-5 pt-5">Making the simple complicated is commonplace.
                            Making the complicated simple, awesomely simple, 
                            that’s creative. 
                    </h5>
                    <p class="font-14 text-center pb-5">-Charles Mingus</p>
                </div>
            </div>
        </section>

<?php get_footer(); ?>