<?php /* Template Name: dsa*/ ?>
<?php get_header(); ?>
        
<main>
	
<section class="w-100 pt-md-5 pt-3 emi-help dsa-partner">
	<div class="container">
		<div class="row">
			<div class="col-12 left-content fc-slate2">
                <h5 class="position-relative fw-600 text-uppercase"><?php the_field('section1_heading'); ?></h5>              
                <p class="fs-14"><?php the_field('section1_desc'); ?></p>
            </div>
                <div class="card col-md-3 col-12" style="width: 18rem;">
                    <div class="card-inner  py-4">                
                        <img src="<?php the_field('sec1_card1_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="emi-help-bg-div"></div>
                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec1_card1_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_field('sec1_card1_desc'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-3 col-12" style="width: 18rem;">
                    <div class="card-inner  py-4">
                        <img src="<?php the_field('sec1_card2_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="emi-help-bg-div"></div>


                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec1_card2_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_field('sec1_card2_desc'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-3 col-12" style="width: 18rem;">
                    <div class="card-inner  py-4">
                        <img src="<?php the_field('sec1_card3_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="emi-help-bg-div"></div>

                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec1_card3_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_field('sec1_card3_desc'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-3 col-12" style="width: 18rem;">
                        <div class="card-inner  py-4">
                            <img src="<?php the_field('sec1_card4_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                            <div class="emi-help-bg-div"></div>
    
                            <div class="card-body p-0">
                              <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec1_card4_heading'); ?></h6>
                              <p class="card-text fc-slate"><?php the_field('sec1_card4_desc'); ?></p>
                            </div>
                        </div>
                    </div>
		</div>
	</div>
</section>


<section class="w-100 our-products pt-md-5 pt-3">
        <div class="container">
            <div class="row">   
<div class="col-12">
<h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section2_heading'); ?></h5>

</div>
                  <div class="rio-promos col-12">
					  <?php if( have_rows('section2_card') ): ?>
					  <?php while( have_rows('section2_card') ): the_row();?>

                          <div class="card" style="width: 18rem;">
							  <a href="<?php the_sub_field('section2_card_link'); ?>">
							 
                                  <div class="position-relative img-div"> 
                                  <img src="<?php the_sub_field('section2_card_image'); ?>" alt="our-products-image" class="card-img-top">
                                  <figcaption><?php the_sub_field('section2_card_heading'); ?></figcaption>
                                  </div>
                                  <div class="card-body pl-0 pr-2">                                        
                                    <p class="card-text fc-slate"><?php the_sub_field('section2_card_desc'); ?></p>
                                  </div>
								   </a>
                                </div>
					  
					  
						<?php endwhile; ?>
					  <?php endif; ?>
                         
                                 
                                    
                                        
                                            
                          
                  </div>

            </div>

        </div>

    </section>
	
	
<section class="w-100 py-md-5 py-3 benefits-product position-relative ">
    <div class="container">
        <div class="row">
            <div class="col-md-5 offset-md-1 left-content pr-md-5 py-3 fc-slate2">
                <h5 class="position-relative fw-600 text-uppercase mb-3"><?php the_field('section4_heading'); ?></h5>              
                <p class="fs-14"><?php the_field('section4_desc'); ?></p>
				<?php if( have_rows('section4_benefits _points') ): ?>
                <ul class="pl-3">					
					
					<?php while( have_rows('section4_benefits _points') ): the_row();?>
					
                    <li class="mb-3 fs-14"><?php the_sub_field('benefits_points'); ?></li>
					
					<?php endwhile; ?>
					                    
                </ul>
				<?php endif; ?>       
            </div>
			
			
            <div class="col-md-5 col-12 right-content py-md-3 ">	  
				<img src="<?php the_field('benefits_image'); ?>" class="w-100" />
                    <!-- <a id="play-video" href="#">Play Video</a><br /> --> 
<!--                     <iframe id="video" width="100%" height="100%" src="//www.youtube.com/embed/<?php the_field('sec4_video_link'); ?>" frameborder="0" allowfullscreen></iframe> -->
            </div>
			
        </div>
    </div>
</section>
            
   <!-- <section class="w-100 our-partners py-md-5 py-3">
            <div class="container">
                <div class="row">   
                    <div class="col-12">
                        <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase text-center"><?php the_field('section3_heading'); ?></h5>
                    </div>
                    <div class="rio-promos-about col-12 mb-0 mt-md-4">
						
						<?php if( have_rows('section3_card') ): ?>
						<?php while( have_rows('section3_card') ): the_row();?>
        
                        <img src="<?php the_sub_field('section3_logos'); ?>" class=""/>
                       
        
						<?php endwhile; ?>
						<?php endif; ?>
        
        
                    </div>
                </div>
            </div>
        </section>                       
    

        <section class="w-100 pt-md-5 pt-3  position-relative rb-bg">
                <div class="container">
                    <div class="row">   
                          
                     
                        <div class="col-md-6 col-12 ">
                                <h5 class="fw-600 pb-3 fc-slate2 text-uppercase"><?php the_field('section4_heading'); ?></h5>

                            <h6 class="fc-slate fw-600"><?php the_field('section4_sub_heading'); ?></h6>
                            <p class="fc-slate2 "><?php the_field('section4_desc'); ?></p>
                        </div>
                        <div class="col-md-4 col-12 offset-md-2">
                            <img src="<?php the_field('section4_image'); ?>" alt="who-we-are" class="w-100"/>
                        </div>    
                                             
                    </div>
                </div>
            </section>

            <section class="w-100 our-products py-md-5 py-3 winners-rr">
                    <div class="container">
                        <div class="row">   
            <div class="col-12">
            <h6 class="fw-600 pl-1 pb-3 fc-slate2 "><?php the_field('section5_heading'); ?></h6>
            
            </div>
                              <div class="rio-promos col-12">
								  
								  <?php if( have_rows('section5_card') ): ?>
								  <?php while( have_rows('section5_card') ): the_row();?>
            
                                    <div class="card">
                                        <div class="position-relative img-div"> 
                                        <img src="<?php the_sub_field('sec5_card_image'); ?>" alt="our-products-image" class="card-img-top">
                                        </div>
                                        <div class="card-body py-2 px-3"> 
                                            <h6 class="fw-600 fc-white"><?php the_sub_field('sec5_card_name'); ?></h6>                                       
                                        <p class="card-text fc-white text-uppercase fs-10"><?php the_sub_field('sec5_card_city'); ?></p>
                                        </div>
                                    </div>
								  
								  <?php endwhile; ?>
								  <?php endif; ?>
								  
								                     
                                      
                              </div>
            
                        </div>
            
                    </div>
            
                </section>

-->  

     <section class="w-100 media py-md-4 py-2 position-relative">
            <div class="container">
                <div class="row">   
							<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-blue-h.png" alt="patern-image" class="ce-m position-absolute d-none d-md-block">
						<img src="<?php echo get_template_directory_uri(); ?>/images/blue-pattern.PNG" alt="patern-image" class="ce-m1 position-absolute d-none d-md-block">
					<div class="position-absolute d-none d-md-block ce-m-bg-div">
						
					</div>
                    <div class="col-12">
                    <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase text-center"><?php the_field('section6_heading'); ?></h5>
                    </div>
                    <div class="rio-promos-product-testimonials col-md-10 col-12 mx-auto">   
						
						<?php if( have_rows('section6_card') ): ?>
						<?php while( have_rows('section6_card') ): the_row();?>
						
                        <div class="card col-md-12 text-center py-5 px-md-4 px-0" style="width: 18rem;">
                            <div class="position-relative img-div"> 
                                <img src="<?php the_sub_field('sec6_card_image'); ?>" alt="our-products-image" class="card-img-top mx-auto pb-3">
                            </div>
                            <div class="card-body px-md-2 px-4">    
                                <h6 class="card-title fc-blue mb-1"><?php the_sub_field('sec6_card_name'); ?></h6>   
                                <span class="text-uppercase fc-slate"><small><?php the_sub_field('sec6_card_city'); ?></small></span>
                                <p class="fc-slate quote-text position-relative pt-5"><?php the_sub_field('sec6_card_desc'); ?></p>
                            </div>
                        </div>
						
						<?php endwhile; ?>
						<?php endif; ?>
                   
                    </div> 
                </div>
            </div>
        </section> 
               
             
<section class="faq-product w-100 py-3">
        <div class="container">
            <div class="row">
                <div class="col-md-10 p-md-5 fc-slate-90 mx-auto">
                    <h5 class="fc-slate fw-600 text-uppercase pb-3 text-center"><?php the_field('section7_heading'); ?></h5>
					<p>
						We are happy to answer your questions. Here is a list of frequently asked queries for your reference. Alternately, you can speak to our customer care officers by dialling 1800 419 0999
					</p>
                    <div class="demo p-3">
<?php if( have_rows('sec7_card') ): ?>
	
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<?php $i=1; while ( have_rows('sec7_card') ) : the_row(); ?>
                    
                            <div class="panel panel-default mb-3">
                                <div class="panel-heading" role="tab" id="headingOne-<?php echo $i; ?>">
                                    <h4 class="panel-title mb-0">
                                        <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne-<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="far fa-plus-square"></i>
                                            
                                            <span class="pl-2"><?php the_sub_field('sec7_card_question'); ?></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne-<?php echo $i; ?>" class="panel-collapse collapse <?php if ($i==1) { echo ''; } ?>" role="tabpanel" aria-labelledby="headingOne-<?php echo $i; ?>">
                                    <div class="panel-body p-4"><?php the_sub_field('sec7_card_answer'); ?></div>
                                </div>
                            </div>
                    
<?php $i++; endwhile; ?>
                        </div><!-- panel-group -->
                        <?php endif; ?>
                        
                    </div><!-- container -->
                    
                   


                </div>
              
            </div>
        </div>
</section>

<section class="w-100 inner-contactus py-md-5 py-3" style="background-image: url(/wp-content/themes/capitalfloat/images/contactus-bg.png)">
	<div class="container">
		<div class="row">
			<div class="col-12 text-container">
				<h5 class="fc-slate fw-600 text-uppercase pb-2">contact us now</h5>
                <p class="fs-14 pb-md-3 pb-2">We are happy to answer all your questions. Contact us for any queries or feedback you have regarding our DSA partner program.</p>				
			</div>
			<div class="col-12">
				<?php echo do_shortcode('[contact-form-7 id="1709" title="contactus-innerpages"]'); ?>				
			</div>
			<div class="col-md-7 d-flex justify-content-center contact-faq">
				<p class="fs-14 fc-slate m-0">
					For any queries, go to <a href="#" class="fs-14 fc-slate fw-800">FAQ’s</a>
				</p>
							
			</div>
			
		</div>		
	</div>
</section>







      </main>
<?php get_footer(); ?>