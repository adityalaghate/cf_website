<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage capitalfloat
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

<section id="primary" class="content-area serachresult-page">
		<main id="main" class="site-main">
			<div class="container">
				<div class="row">
					<div class="col-12">						
					
<?php
	get_search_form();
?>
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title d-inline-flex">
					<?php _e( 'Search results for:', 'twentynineteen' ); ?>
				</h1>
				<span class="page-description fs-20 search-word"><?php echo get_search_query(); ?></span>
			</header><!-- .page-header -->

			<?php
			// Start the Loop.
			while ( have_posts() ) :
				the_post();?>
						<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
						<p>
							<?php echo the_excerpt();?>
						</p>
			<?php	
			endwhile;

			// Previous/next page navigation.

			// If no content, include the "No posts found" template.
		else :
						echo 'No results found';
			get_template_part( 'template-parts/content/content', 'none' );

		endif;
		?>
						</div>					
				</div>				
			</div>
		</main><!-- #main -->
	</section><!-- #primary -->


<p class="black1 font-weight-600 w-100 mt-4" id="faq_28"><?php foreach(explode("|", get_query_var( 'category_name' )) as $cname) {echo get_category_by_slug( $cname )->name.', '; }?></p>
		<?php
$s=get_search_query();
// echo $s;


//$args = array(
//                's' =>$s,
 				//'category_name'=> implode(',',explode('|', get_query_var( 'category_name' )))
 //           );
    // The Query

//$the_query = new WP_Query( $args );
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'content', 'excerpt' );

			endwhile; // End of the loop.

		else : ?>

<!-- 			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'capitalfloat' ); ?></p> -->
			<?php
// 				get_search_form();

		endif;
		?>
<?php
get_footer();
?>
