<?php /* Template Name: regulatory */ ?>
<?php get_header(); ?>
        
<main>
	
	
<section class="w-100 regulatory py-md-5 py-3 my-md-5 my-3">
    <div class="container">
        <div class="row">
			<?php if( have_rows('card') ): ?>
			<?php while( have_rows('card') ): the_row();?>
						
                <div class="card col-md-3 col-12 text-center">
					<a href="<?php the_sub_field('card_pdf_link'); ?>" target="_blank">
                    <div class="card-inner my-3 p-3 h-100">
                        <img class="card-img-top mx-auto" src="<?php the_sub_field('card_image'); ?>" alt="Card image cap">
                        <div class="card-body pb-0">
                            <h6 class="card-title fw-600 fc-slate2 m-0"><?php the_sub_field('card_heading'); ?></h6>                                
                        </div>
                    </div>
						</a>
                </div>				
				
				<?php endwhile; ?>
			<?php endif; ?>		
                                  
        </div>
    </div>
</section> 






      </main>
<?php get_footer(); ?>