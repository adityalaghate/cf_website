<?php /* Template Name: corporate-enterprise */ ?>
<?php get_header(); ?>
        
<main>
	
        <section class="w-100 py-3 py-md-5 team-what-we-do">
                <div class="container">
                    <div class="row">   
                        <div class="col-12 ">
                            <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section1_heading'); ?></h5>
                           
                        </div>
                        <div class="card col-md-4 col-12" style="width: 18rem;">
                            <div class="card-inner">                
                                <img src="<?php the_field('sec1_card1_image'); ?>" class="pb-md-4 pb-3 w-100" alt="...">
                                <div class="card-body p-0">
                                  <h6 class="card-title fc-slate fw-600 lh-24 pr-5 mb-1"><?php the_field('sec1_card1_heading'); ?></h6>
                                  <p class="card-text fc-slate"><?php the_field('sec1_card1_desc'); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="card col-md-4 col-12" style="width: 18rem;">
                            <div class="card-inner">
                                <img src="<?php the_field('sec1_card2_image'); ?>" class="pb-md-4 pb-3 w-100" alt="...">
                                <div class="card-body p-0">
                                  <h6 class="card-title fc-slate fw-600 lh-24 pr-5 mb-1"><?php the_field('sec1_card2_heading'); ?></h6>
                                  <p class="card-text fc-slate"><?php the_field('sec1_card2_desc'); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="card col-md-4 col-12" style="width: 18rem;">
                            <div class="card-inner">
                                <img src="<?php the_field('sec1_card3_image'); ?>" class="pb-md-4 pb-3 w-100" alt="...">
                                <div class="card-body p-0">
                                  <h6 class="card-title fc-slate fw-600 lh-24 pr-5 mb-1"><?php the_field('sec1_card3_heading'); ?></h6>
                                  <p class="card-text fc-slate"><?php the_field('sec1_card3_desc'); ?></p>
                                </div>
                            </div>
                        </div>
                                
                    </div>
                </div>
        </section>

           <section class="w-100 pt-md-5 pt-3 pb-md-4 pb-2 corporate">
                <div class="container py-3">
                    <div class="row">  
                        <div class="col-12 left-content fc-slate2">
                            <h5 class="position-relative fw-600 text-uppercase mb-3"><?php the_field('section2_heading'); ?></h5>              
                            <p class="fs-14 pb-1"><?php the_field('section2_desc'); ?></p>
                        </div>
						
                      <?php if( have_rows('section2_card') ): ?>
						<?php while( have_rows('section2_card') ): the_row();?>
						
                        <div class="card col-md-4 col-12 mb-4" style="width: 18rem;">
                            <div class="card-inner  p-4">                
                                <img src="<?php the_sub_field('sec2_card_image'); ?>" class="pb-md-4 pb-3" alt="...">
                                <div class="card-body p-0">
                                  <h6 class="card-title fc-slate fw-600 lh-24"><?php the_sub_field('sec2_card_heading'); ?></h6>
                                  <p class="card-text fc-slate"><?php the_sub_field('sec2_card_desc'); ?></p>
                                </div>
                            </div>
                        </div>
						
							<?php endwhile; ?>
						<?php endif; ?>
            
          
                
         
         
                                
                    </div>
                </div>
            </section>


            <section class="w-100 our-products py-md-5 py-3 mt-md-3">
                    <div class="container">
                        <div class="row">   
      <div class="col-12">
          <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section3_heading'); ?></h5>
      
      </div>
                              <div class="rio-promos col-12">
								  
								  <?php if( have_rows('section3_card') ): ?>
								  <?php while( have_rows('section3_card') ): the_row();?>
								  
      
								  <div class="card" style="width: 18rem;">
									  <div class="position-relative img-div"> 
										  <img src="<?php the_sub_field('sec3_card_image'); ?>" alt="our-products-image" class="card-img-top">
										  <figcaption><?php the_sub_field('sec3_card_heading'); ?></figcaption>
									  </div>
									  <div class="card-body pl-0 pr-2">                                        
										  <p class="card-text fc-slate"><?php the_sub_field('sec3_card_desc'); ?></p>
									  </div>
								  </div>
								  
								  	<?php endwhile; ?>



<?php endif; ?>

                                       
                                           
                                             
                                                        
                                      
                              </div>
      
                        </div>
      
                    </div>
      
                </section>
      
                <section class="w-100 our-partners py-md-5 py-3">
                        <div class="container">
                            <div class="row">   
                                <div class="col-12">
                                    <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase text-center"><?php the_field('section4_heading'); ?></h5>
                                </div>
                                <div class="rio-promos-about col-12 mb-0 mt-md-4">
									
									<?php if( have_rows('sec4_card') ): ?>
									<?php while( have_rows('sec4_card') ): the_row();?>
									
                    
                                    <img src="<?php the_sub_field('sec4_card_logos'); ?>" class=""/>
                                   <?php endwhile; ?>
									<?php endif; ?>
                    
                    
                                </div>
                            </div>
                        </div>
                    </section>    
      
                    <section class="w-100 py-5 c-float-numbers">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-5 col-12 offset-md-1 left-content pr-5 fc-slate2">
                                        <h5 class="position-relative"><?php the_field('section5_heading'); ?></h5>
                                        <hr></hr>
                                        <p class="fs-14"><?php the_field('section5_desc'); ?></p>
                                    </div>
                                    <div class="col-md-6 col-12 right-content">				
                                            <div class="row fs-15">
                                                <div class="col-md-5 content">
                                                        <div class="mb-3">
                                                            <p><?php the_field('section5_card1_number'); ?></p>
                                                            <span class="span-fw-600"><?php the_field('section5_card1_city'); ?></span>
                                                        </div>
                                                </div>
                                                <div class="col-md-5 content pl-md-0">
                                                        <div class="mb-3">
                                                            <p><?php the_field('section5_card2_number'); ?></p>
                                                            <span class="span-fw-600"><?php the_field('section5_card2_city'); ?></span>
                                                        </div>
                                                </div>
                                                <div class="col-md-5 content">
                                                        <div class="mb-3">
                                                            <p><sub class="cf-sub">+</sub><?php the_field('section5_card3_number'); ?></p>
                                                            <span class="span-fw-600"><?php the_field('section5_card3_city'); ?></span>
                                                        </div>
                                                </div>
                                                <div class="col-md-5 content pl-md-0">
                                                        <div class="mb-3"> 
                                                            <p><sub class="cf-sub">+</sub><?php the_field('section5_card4_number'); ?></p>
                                                            <span class="span-fw-600"><?php the_field('section5_card4_city'); ?></span>
                                                        </div>
                                                </div>
                                            </div>	
                                        </div>
                                </div>
                            </div>
                    </section>

                    
    


    <section class="w-100 media py-md-4 py-2 position-relative">
            <div class="container">
                <div class="row">   
							<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-blue-h.png" alt="patern-image" class="ce-m position-absolute d-none d-md-block">
						<img src="<?php echo get_template_directory_uri(); ?>/images/blue-pattern.PNG" alt="patern-image" class="ce-m1 position-absolute d-none d-md-block">
					<div class="position-absolute d-none d-md-block ce-m-bg-div">
						
					</div>
                    <div class="col-12">
                    <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase text-center"><?php the_field('section6_heading'); ?></h5>
                    </div>
                    <div class="rio-promos-product-testimonials col-10 mx-auto"> 
						
						<?php if( have_rows('sec6_card') ): ?>
					<?php while( have_rows('sec6_card') ): the_row();?>

						
						
                        <div class="card col-md-12 text-center py-5 px-md-4 px-0" style="">
                            <div class="position-relative img-div"> 
                                <img src="<?php the_sub_field('sec6_card_image'); ?>" alt="our-products-image" class="card-img-top mx-auto pb-3">
                            </div>
                            <div class="card-body px-md-2 px-4">    
                                <h6 class="card-title fc-blue mb-1"><?php the_sub_field('sec6_card_name'); ?></h6>   
                                <span class="text-uppercase fc-slate"><small><?php the_sub_field('sec6_card_city'); ?></small></span>
                                <p class="fc-slate quote-text position-relative pt-5"><?php the_sub_field('sec6_card_desc'); ?></p>
                            </div>
                        </div>
						
						<?php endwhile; ?>
						<?php endif; ?>
                     
                 
                    </div> 
                </div>
            </div>
        </section>  

        









                 
             
<section class="faq-product w-100 pt-md-5 pt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-10 p-5 fc-slate-90 mx-auto">
                    <h5 class="fc-slate fw-600 text-uppercase pb-3 text-center"><?php the_field('section7_heading'); ?></h5>
                    <div class="demo p-3">

	<?php if( have_rows('sec7_card') ): ?>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<?php $i=1; while ( have_rows('sec7_card') ) : the_row(); ?>
                    
                            <div class="panel panel-default mb-3">
                                <div class="panel-heading" role="tab" id="headingOne-<?php echo $i; ?>">
                                    <h4 class="panel-title mb-0">
                                        <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne-<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="far fa-plus-square"></i>
                                            <!-- <i class="far fa-minus-square"></i> -->
                                            <span class="pl-3"><?php the_sub_field('sec7_card_question'); ?></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne-<?php echo $i; ?>" class="panel-collapse collapse <?php if ($i==1) { echo 'show'; } ?>" role="tabpanel" aria-labelledby="headingOne-<?php echo $i; ?>">
                                    <div class="panel-body p-4"><?php the_sub_field('sec7_card_answer'); ?></div>
                                </div>
                            </div>                
                    
                    
                         <?php $i++; endwhile; ?>
                    
                        </div><!-- panel-group -->
						<?php endif; ?>
                        
                        
                    </div><!-- container -->
                    
                   


                </div>
              
            </div>
        </div>
</section>

<section class="w-100 inner-contactus py-md-5 py-3" style="background-image: url(/wp-content/themes/capitalfloat/images/contactus-bg.png)">
	<div class="container">
		<div class="row">
			<div class="col-12 text-container">
				<h5 class="fc-slate fw-600 text-uppercase pb-2">contact us now</h5>
                <p class="fs-14 pb-md-3 pb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>				
			</div>
			<div class="col-12">
				<?php echo do_shortcode('[contact-form-7 id="1709" title="contactus-innerpages"]'); ?>				
			</div>
			<div class="col-md-7 d-flex justify-content-center contact-faq">
				<p class="fs-14 fc-slate m-0">
					For any queries, go to <a href="#" class="fs-14 fc-slate fw-800">FAQ’s</a>
				</p>
							
			</div>
			
		</div>		
	</div>
</section>



      </main>
<?php get_footer(); ?>