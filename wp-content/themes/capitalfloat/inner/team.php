<?php /* Template Name: team */ ?>
<?php get_header(); ?>

<main>
<section class="w-100 py-md-5 py-3">
    <div class="container">
        <div class="row">   
            <div class="col-12">
            </div>
            <div class="col-md-4 col-12 pb-3 pb-md-3">
                <img src="<?php the_field('section1_image'); ?>" alt="who-we-are" class="w-100"/>
            </div>
            <div class="col-md-6 col-12">
                    <h5 class="pl-md-5 fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section1_heading'); ?></h5>

                <p class="pl-md-5 fc-slate2"><?php the_field('section1_desc'); ?></p>
            </div>
        </div>
    </div>
</section>
<section class="w-100 pb-3 our-values">
        <div class="container py-md-3 py-0">
            <div class="row">  
                <div class="col-12 text-center pb-md-3 pb-0">
                    <h5 class="fw-600 pl-1  fc-slate2 text-uppercase"><?php the_field('section2_heading'); ?></h5>
                </div>
				
				<?php if( have_rows('section2_card') ): ?>



	<?php while( have_rows('section2_card') ): the_row();?>
		
                <div class="card col-md-4 col-12 text-center" style="width: 18rem;">
                    <div class="card-inner  p-4">                
                        <img src="<?php the_sub_field('sec2_card_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="our-value-bg-div"></div>
                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24"><?php the_sub_field('sec2_card_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_sub_field('sec2_card_desc'); ?></p>
                        </div>
                    </div>
                </div>
				
				<?php endwhile; ?>

	

<?php endif; ?>
          
   
                        
            </div>
        </div>
    </section>


<section class="w-100 py-3 team-what-we-do">
        <div class="container">
            <div class="row">   
                <div class="col-12 pb-md-3 pb-0">
                    <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section3_heading'); ?></h5>
                   
                </div>
				
				<?php if( have_rows('section3_card') ): ?>
				<?php while( have_rows('section3_card') ): the_row();?>
		
                <div class="card col-md-4 col-12 pb-md-0 pb-3" style="width: 18rem;">
                    <div class="card-inner">                
                        <img src="<?php the_sub_field('sec3_card_image'); ?>" class="pb-md-4 pb-3 w-100" alt="...">
                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 pr-5"><?php the_sub_field('sec3_card_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_sub_field('sec3_card_desc'); ?></p>
                        </div>
                    </div>
                </div>
				
				<?php endwhile; ?>
				<?php endif; ?>
    
   
                        
            </div>
        </div>
</section>




<section class="w-100 team-testimonials py-md-5 py-3 my-md-5 my-3" style="background-image: url(<?php the_field('section4_bg'); ?>)" id="team-testimonial-bg-images" >
<!-- 	style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/team-testimonials.png)" -->
    <div class="container">
        <div class="row">   
            <div class="col-12">
                <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section4_heading'); ?></h5>
            </div>
            <div class="rio-promos-teams-testimonials col-md-6 col-12">
				
				<?php if( have_rows('section4_card') ): ?>



	<?php while( have_rows('section4_card') ): the_row();?>
		
                <div class="card p-md-5 p-0 mt-5" style="width: 18rem;">
                    <div class="card-body pl-md-0 pr-md-2 ">
                        <p class="card-text fc-slate"><?php the_sub_field('section4_card_text'); ?></p>
                    </div>
                </div>  
				
				<?php endwhile; ?>
<?php endif; ?>
                                        
            </div>         
        </div>
    </div>
</section>




<section class="w-100 team-leadership py-3 ">
        <div class="container">
            <div class="row">   
<div class="col-12">
<h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section5_heading'); ?></h5>

</div>
                <div class="rio-promos col-12">
					<?php if( have_rows('section5_card') ): ?>



	<?php while( have_rows('section5_card') ): the_row();?>
					
                    <div class="card px-1" style="width: 18rem;">
                         <div class="position-relative img-div"> 
                            <img src="<?php the_sub_field('sec5_card_image'); ?>" alt="leadership-image" class="card-img-top">
                        </div>
                        <div class="card-body p-0">      
							 <div class="team-cardbody-parent position-relative">
								 <div class="p-3">
									  <h6 class="card-title"><?php the_sub_field('sec5_card_heading'); ?></h6>                                  
                            <p class="card-text fc-slate pr-5"><?php the_sub_field('sec5_card_desc'); ?></p> 
									 
								 </div>
								          <div class="team-btn-grp d-flex">                                     
                            <a href="<?php the_sub_field('sec5_twitter_link'); ?>" target="_blank" class="btn team-twitter"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter-team.png" target="_blank" class="mx-auto" /></a>
                            <a href="<?php the_sub_field('sec5_linkedin_link'); ?>" class="btn team-linkedin"><img src="<?php echo get_template_directory_uri(); ?>/images/linkedin-team.png" class="mx-auto" /></a>                                      
                        </div>
								 
							</div>
                                                             
                        </div>
               
                    </div>
					
					<?php endwhile; ?>


<?php endif; ?>
        
              
                    
                                     
                </div>
            </div>

        </div>

</section>


  </main>
<?php get_footer(); ?>
