<?php /* Template Name: homepage */ ?>
<?php get_header(); ?>
<main>
<section class="w-100 our-products h-op our-products-bg py-md-5 py-3 position-relative animation-element slide-up">
	<div class="container pb-md-5 ">
		<div class="row">   
			<img src="<?php echo get_template_directory_uri(); ?>/images/pattern.png" alt="patern-image" class="product-pattern w-25 position-absolute d-none d-md-block">
			<div class="col-12">
				<h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section1_heading'); ?></h5>
			</div>
			<div class="rio-promos col-12">				
				<?php if( have_rows('section1_card') ): ?>
				<?php while( have_rows('section1_card') ): the_row();?>				
				<div href="/product/">
					<a href="<?php the_sub_field('card_link'); ?>">
						<div class="card" >
							<div class="position-relative img-div"> 
								<img src="<?php the_sub_field('sec1_card_image'); ?>" alt="our-products-image" class="card-img-top">
								<figcaption><?php the_sub_field('sec1_card_heading'); ?></figcaption>
							</div>
							<div class="card-body pl-0 pr-2">                                        
								<p class="card-text fc-slate"><?php the_sub_field('sec1_card_desc'); ?></p>
							</div>
						</div>
					</a>					
				</div>				
				<?php endwhile; ?>
				<?php endif; ?>                                
			</div>
		</div>
	</div>
</section>

<section class="w-100 exclusive-benefits text-white animation-element slide-up">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-12 bg-light-blue py-3 py-md-5">
				<h5 class="pr-3 position-relative"><?php the_field('section2_heading'); ?></h5>

			</div>
			<div class="col-md-3 col-12 bg-dark-gray text-center  py-3  py-md-5">
				<img src="<?php the_field('sec2_card1_image'); ?>" alt="collateral-free"/>
				<p class="fs-15 mb-2"><?php the_field('sec2_card1_heading'); ?></p>
				<p class="m-0 fs-13 px-3"><?php the_field('sec2_card1_desc'); ?></p>

			</div>
			<div class="col-md-3 col-12 bg-dark-gray text-center  py-3  py-md-5">
				<img src="<?php the_field('sec2_card2_image'); ?>" alt="online-application"/>
				<p class="fs-15 mb-2"><?php the_field('sec2_card2_heading'); ?></p>
				<p class="m-0 fs-13 px-3"><?php the_field('sec2_card2_desc'); ?></p>
			</div>
			<div class="col-md-3 col-12 bg-dark-gray text-center  py-3  py-md-5">
				<img src="<?php the_field('sec2_card3_image'); ?>" alt="money"/>
				<p class="fs-15 mb-2"><?php the_field('sec2_card3_heading'); ?></p>
				<p class="m-0 fs-13 px-3"><?php the_field('sec2_card3_desc'); ?></p>      
			</div>       
		</div>
	</div>
</section>

        <section class="w-100 py-md-5 py-3 c-float-numbers position-relative animation-element slide-up">
              <div class="container">
                <div class="row">
					<img src="<?php echo get_template_directory_uri(); ?>/images/blue-pattern.PNG" alt="patern-image" class="c-float-pattern position-absolute d-none d-md-block">
                  <div class="col-md-5 col-12 offset-md-1 left-content pr-2 pr-md-5 fc-slate2">
                    <h5 class="position-relative"><?php the_field('section3_heading'); ?></h5>
                    <hr></hr>
                    <p class="fs-14"><?php the_field('section3_desc'); ?></p>
                  </div>
                  <div class="col-md-6 col-12 right-content " id="counter">       
                      <div class="row fs-15">
                        <div class="col-6 col-md-5 content pr-md-0">
                            <div class="mb-3">
								<p><span class="counter-value" data-count="<?php the_field('sec3_card1_number'); ?>">0</span><span>+</span></p>
                              <span><?php the_field('sec3_card1_text'); ?></span>
                            </div>
                        </div>
                        <div class="col-6 col-md-5 content offset-md-1 pl-md-0">
                            <div class="mb-3">
								<p><span class="counter-value" data-count="<?php the_field('sec3_card2_number'); ?>">0</span><span>+ Cr</span></p>
                              <span><?php the_field('sec3_card2_text'); ?></span>
                            </div>
                        </div>
                        <div class="col-6 col-md-5 content pr-md-0">
                            <div class="mb-3">
                              <p><span class="counter-value" data-count="<?php the_field('sec3_card3_number'); ?>">0</span><span>%</span></p>
                              <span><?php the_field('sec3_card3_text'); ?></span>
                            </div>
                        </div>
                        <div class="col-6 col-md-5 content offset-md-1 pl-md-0">
                            <div class="mb-3"> 
                              <p><span class="counter-value" data-count="<?php the_field('sec3_card4_number'); ?>">0</span><span>+</span></p>
                              <span><?php the_field('sec3_card4_text'); ?></span>
                            </div>
                        </div>
                      </div>  
                    </div>
                </div>
              </div>
        </section>

        <section class="w-100 get-a-sanction py-md-5 py-3 my-md-5 my-0 position-relative animation-element slide-up">
          <section class="container py-3">
            <div class="row">
				<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-h.png" alt="get-a-sanction-image" class="get-a-sanction-pattern position-absolute d-none d-md-block">
                                <div class="col-md-5 col-12 offset-md-1 left-content pr-5 fc-slate2">
                    <h5 class="position-relative"><?php the_field('section4_heading'); ?></h5>
                    
                                        <p class="fs-14"><?php the_field('section4_desc'); ?></p>
                                        <div class="button-wrap pt-3">
                                           
											 <a href="/walnut" class="btn-apply"><?php the_field('section4_btn_text'); ?></a>
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-12 right-content position-relative d-flex justify-content-center">       
                      <img src="<?php the_field('section4_image'); ?>"  class="d-none d-md-block phone-img position-absolute" alt="galaxy" />
                    </div>

            </div>

          </section>          
                </section>

                <section class="w-100 media pt-md-5 pt-3 mt-md-5 mt-3 animation-element slide-up">
                        <div class="container">
                            <div class="row">   
          <div class="col-12">
              <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section5_heading'); ?></h5>
          
          </div>
								<div class="rio-promos-media col-12">									  
									<?php if( have_rows('section5_card') ): ?>
									<?php while( have_rows('section5_card') ): the_row();?>						  
										<div class="card" style="width: 18rem;">
											<a href="<?php the_sub_field('sec5_card_link'); ?>" target="_new">     
												<div class="position-relative img-div"> 
													<img src="<?php the_sub_field('sec5_card_image'); ?>" alt="our-products-image" class="card-img-top">
												</div>
												<div class="card-body pl-0 pr-5 pt-3">    
													<h6 class="card-title fc-slate pr-5 lh-24"><?php the_sub_field('sec5_card_text'); ?></h6>		 </div>
											</a>
										</div>
									<?php endwhile; ?>
									<?php endif; ?>
								</div>
                            </div>
                        </div>
          
                    </section>

                    
<section class="w-100 awards py-md-5 py-3   animation-element slide-up">
    <div class="container">
        <div class="row">   
            <div class="col-12 pb-md-4 pb-2">
              <h5 class="fw-600 pl-1 pb-3 text-uppercase fc-white"><?php the_field('section6_heading'); ?></h5>          
             </div>
            <div class="rio-promos-awards col-12 d-flex justify-content-stretch">
				
				<?php if( have_rows('section6_card') ): ?>
				<?php while( have_rows('section6_card') ): the_row();?>
				
				
                <div class="card text-center p-md-4 py-4 py-md-0 " style="width: 18rem;">                    
                    <div class="position-relative img-div"> 
                        <img src="<?php the_sub_field('sec6_card_image'); ?>" alt="our-products-image" class="card-img-awards mx-auto">
                    </div>
                    <div class="card-body pt-3 pb-0">    
                        <h5 class="card-title fc-slate fw-600 mb-1 "><?php the_sub_field('sec6_card_year'); ?></h5>    
                        <span class="fc-gray"><?php the_sub_field('sec6_card_awardedby'); ?></span>
                      <!--  <p class="fc-blue text-uppercase ls-1 fw-600 pt-1 m-0"><?php the_sub_field('sec6_card_award_name'); ?></p>-->
                    </div>
            </div>
    <?php endwhile; ?>

	

<?php endif; ?>
   
                              
            </div>
        </div>
    </div>
</section>
<section class="business-loan w-100 animation-element slide-up">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 p-md-5 p-3 fc-slate-90">
                                    <h3 class="fc-slate2"><?php the_field('section7_heading'); ?></h3>
                                    <p><?php the_field('section7_desc'); ?></p>
                                   
                                    <div class="button-wrap pt-md-3 pt-0">
                                        <button class="btn-apply mb-3 mb-md-0 "><?php the_field('section7_applynow_btn_text'); ?></button>
                                        <button class="btn-cancel"><?php the_field('section7_cancel_btn_text'); ?></button>
                                    </div>
                                </div>
                                <div class="col-md-3 d-none d-md-block">
                                    <div class="text-center p-5">
                                        <img src="<?php the_field('section7_image'); ?>" alt="Logo Pattern" width="100%"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>



<section class="w-100">
            <div class="container">           
                <div class=" row pt-3">
					  <div class="col-12 pb-md-4 pb-2">
                              <div class="row">
                                                <h5 class="fw-600 text-uppercase fc-slate col-6 pl-md-3 mb-1"><?php the_field('section8_heading'); ?></h5>   
                                                <a href="blog" class="ls-08 fc-blue text-uppercase col-6 d-flex justify-content-end fw-600 fs-12 align-items-center">View all</a>
                                        </div>                   
                                     </div>
					
<?php
$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;

$args = array( 'paged'=> $paged, 'posts_per_page' => 3);
query_posts($args);
//foreach( $recent_posts as $recent ){?>
<?php if ( have_posts() ) : while (have_posts()) : the_post(); 
					?>
	<div class="col-md-4 d-flex justify-content-stretch mb-md-4 mb-3">
			<div class="card ">
				<img class="card-img-top" src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'full');?>" alt="Card image cap w-100"/>
				<div class="card-body">
					<figcaption class="small text-left fs-13 figure-caption pb-2"><?php echo get_the_date(); ?></figcaption>

					<h5 class="card-title text-left fs-16">
						<a href="<?php echo get_permalink(get_the_ID()); ?>"><?php echo get_the_title(); ?></a></h5>
					
						<div class="card-text text-left fs-13 line-clamp"><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php echo get_the_content();?></a>
					</div>


				</div>
			</div>
	 </div>
<?php //}?>      
<?php endwhile; ?>
<!-- pagination -->
<?php
global $wp_query;
echo paginate_links( array(
	'base' => '/blog/%_%',
	'format' => '%#%/',
	'current' => max( 0, get_query_var('paged')),
	'total' => $wp_query->max_num_pages,
	'type'=> 'list'
) );					
?>
<?php else : ?>
<!-- No posts found -->
<?php endif; ?>
	 </div>
            </div>
       
        </section>    



           


<section class="w-100 bg-light-blue-grey advertisement animation-element slide-up">
	<div class="container">
		<div class="row">
			<div class="col-md-10 offset-md-1 col-sm-12 p-md-5 p-3" >
				<!--  <h3 class="fc-slate"><?php the_field('section9_heading'); ?></h3>
<p class="fc-slate fs-12"><?php the_field('section9_desc'); ?></p>-->
				<img src="/wp-content/uploads/2019/06/hdfc.png" width="100%" />
<!-- 				<img src="<?php the_field('section9_image'); ?>" width="100%" /> -->
			</div>
		</div>
	</div>
</section>

      </main>
<?php get_footer(); ?>
