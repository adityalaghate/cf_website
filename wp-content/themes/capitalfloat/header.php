<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Capital Float</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	 <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-46880415-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-46880415-1');
</script>
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/fav-logo.png" type="image/x-icon" />
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
	     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.9.0/css/all.css" integrity="sha384-i1LQnF23gykqWXg6jxC2ZbCbUMxyw5gLZY6UiUS98LYV5unm8GWmfkIS6jqJfb4E" crossorigin="anonymous">
		 <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
   		 <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_template_directory_uri(); ?>/css/slick.css" />
	 <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_template_directory_uri(); ?>/css/range-slider.css" />
   		 <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_template_directory_uri(); ?>/css/main.css" />
<!-- 		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css"> -->
	<?php wp_head();?>
	
</head>
<body <?php body_class($class); ?>>
<div class="w-100 fixedContainer">
<div class="container">
                <div class="row">
                       
                        <header class="col-12 py-2">
                                <div class="col-sm-12 d-flex justify-content-end top-nav ">                    
                                        <a href="/contact-us">Contact Us</a>
                                        <a href="#0">Login</a>
                                    </div>
                                <nav class="navbar navbar-default navbar-expand-lg navbar-dark px-0">
                                        <a class="navbar-brand" href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo"/></a>
                                        <button class="navbar-toggler p-0 mr-0" type="button" data-toggle="collapse" data-target="#mySidenav" aria-controls="mySidenav" aria-expanded="false" aria-label="Toggle navigation" onclick="openNav()">
                                           <div id="navicon"> <span></span>
                     <span></span>
                     <span></span>
											      
                  </div>
                                        </button>
									
                                        <div class="navbar-collapse collapse sidenav " id="mySidenav">
                                            <ul class="navbar-nav ml-auto">
												<li class="dropdown nav-item px-3">
  <a class="dropdown-toggle nav-link" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Business Finance
  </a>
  <div class="dropdown-menu fs-14" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="/term-finance">Term Finance</a>
    <a class="dropdown-item" href="/loans-against-card-swipes">Loans Against Card Swipes</a>
    
	<!-- <a class="dropdown-item" href="/doctor-loans">Doctor Loans</a>-->
	 <a class="dropdown-item" href="/school-finance">School Finance</a>

  </div>
</li>

                                               <li class="dropdown nav-item px-3">
  <a class="dropdown-toggle nav-link" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Consumer Finance
  </a>
  <div class="dropdown-menu fs-14" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="/online-checkout-finance">Online Checkout Finance</a>
    <a class="dropdown-item" href="/merchant-checkout-finance">Merchant Checkout Finance</a>
    <a class="dropdown-item" href="/walnut">Walnut</a>

  </div>
</li>
  <li class="dropdown nav-item px-3">
  <a class="dropdown-toggle nav-link" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Partner with Us
  </a>
  <div class="dropdown-menu fs-14" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="/dsa">For DSA</a>
	  <a class="dropdown-item" href="/enterprise">For Enterprise</a>
    <a class="dropdown-item" href="/consumer-loans">For Consumer Loans</a>
    <a class="dropdown-item" href="/co-lending">For Co-lending</a>
	  
  </div>
</li>
    <li class="dropdown nav-item px-3">
  <a class="dropdown-toggle nav-link" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Company
  </a>
  <div class="dropdown-menu fs-14" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="/about-us">About Us</a>
<a class="dropdown-item" href="/team">Team</a>
	  <a class="dropdown-item" href="/media-awards">Media &amp; Awards</a>
	  <a class="dropdown-item" href="/contact-us">Contact Us</a>
	
  </div>
</li>
<li class="nav-item px-3">
  <a class="nav-link" href="/blog" >Blog</a>
</li>
												 <li class="nav-item px-3">
                                                                <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal" data-backdrop="false"><i class="fas fa-search"></i></a>
                                                            </li>  

<!-- 												<img src="<?php echo get_template_directory_uri(); ?>/images/search-magnifier-tool-outline.png" alt="search-icon" class="head-search" /> -->
                                            </ul>
                                           <a href="https://safe.capitalfloat.com/cf/default/register" target="_new"><button class="btn btn-primary my-2 my-sm-0 header-applynow ml-2" type="submit">Apply Now</button></a> 
                
                                        
                                        </div>
                                    </nav>
                                </header>
                </div>
            </div>
	</div>
	
	
	<?php if ( is_page('home') ): ?>
	<section class="w-100 home-banner" >
    <div class="container">
            <div class="row">
                   
                    <div class="bd-example w-100 d-none d-sm-block">
                            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                              <ol class="carousel-indicators">
                                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                <!--<li data-target="#carouselExampleCaptions" data-slide-to="2"></li>-->
                              </ol>
                              <div class="carousel-inner">
                                <div class="carousel-item active" style="background-image:url(<?php the_field('slide1_image'); ?>)">
<!--                                   <img src="<?php the_field('slide1_image'); ?>" class="d-block w-100" alt="..."> -->
                                  <div class="carousel-caption d-md-block">
                                    <h2 class="text-uppercase pr-md-5 font-weight-bold"><?php the_field('slide1_heading'); ?></h2>
                                  <!--  <p class=" ls-08"><?php the_field('slide1_desc'); ?></p>-->
                                    <div class="button-wrap pt-3 col-12 pl-0">
                                            <a class="btn-apply white-text" href="<?php the_field('slide1_btn_link'); ?>"><?php the_field('slide1_btn_text'); ?></a>
										
                                        </div> 
                                  </div>
                                </div>
                                <div class="carousel-item" style="background-image:url(<?php the_field('slide2_image'); ?>)">
<!--                                         <img src="<?php the_field('slide2_image'); ?>" class="d-block w-100" alt="..."> -->
                                        <div class="carousel-caption d-block">
                                          <h2 class="text-uppercase pr-md-5 font-weight-bold"><?php the_field('slide2_heading'); ?></h2>
                                         <!-- <p class=" ls-08"><?php the_field('slide2_desc'); ?></p>-->
                                          <div class="button-wrap pt-3 col-12 pl-0">
                                                    <a class="btn-apply white-text" href="<?php the_field('slide2_btn_link'); ?>"><?php the_field('slide2_btn_text'); ?></a>
                                              </div> 
                                        </div>
                                      </div>
                                      
                         
                              </div>
                              <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/left-arrow-circular-button.png" />
                                
                              </a>
                              <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/right-arrow-circular-button.png" />
                              </a>
                            </div>
                          </div>
				
				
				
				
				
				 <div class="bd-example w-100 d-block d-sm-none">
                            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                              <ol class="carousel-indicators">
                                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                <!--<li data-target="#carouselExampleCaptions" data-slide-to="2"></li>-->
                              </ol>
                              <div class="carousel-inner">
                                <div class="carousel-item active" style="background-image:url(<?php the_field('banner_mobile_bg1'); ?>)">
<!--                                   <img src="<?php the_field('slide1_image'); ?>" class="d-block w-100" alt="..."> -->
                                  <div class="carousel-caption d-md-block">
                                    <h2 class="text-uppercase pr-md-5 font-weight-bold"><?php the_field('slide1_heading'); ?></h2>
                                  <!--  <p class=" ls-08"><?php the_field('slide1_desc'); ?></p>-->
                                    <div class="button-wrap pt-3 col-12 pl-0">
                                            <a class="btn-apply white-text" href="<?php the_field('slide1_btn_link'); ?>"><?php the_field('slide1_btn_text'); ?></a>
										
                                        </div> 
                                  </div>
                                </div>
                                <div class="carousel-item" style="background-image:url(<?php the_field('banner_mobile_bg2'); ?>)">
<!--                                         <img src="<?php the_field('slide2_image'); ?>" class="d-block w-100" alt="..."> -->
                                        <div class="carousel-caption d-block">
                                          <h2 class="text-uppercase pr-md-5 font-weight-bold"><?php the_field('slide2_heading'); ?></h2>
                                         <!-- <p class=" ls-08"><?php the_field('slide2_desc'); ?></p>-->
                                          <div class="button-wrap pt-3 col-12 pl-0">
                                                    <a class="btn-apply white-text" href="<?php the_field('slide2_btn_link'); ?>"><?php the_field('slide2_btn_text'); ?></a>
                                              </div> 
                                        </div>
                                      </div>
                                      
                         
                              </div>
                              <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/left-arrow-circular-button.png" />
                                
                              </a>
                              <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/right-arrow-circular-button.png" />
                              </a>
                            </div>
                          </div>
				
				
				
				
				
				
              
                
            </div>

    </div>
  </section>
<?php elseif ( is_single() ) : ?>
	
	<?php elseif ( is_page('online-checkout-finance') || is_page('walnut') ): ?>
	
		<section class="w-100 banner banner-all web-bg d-none d-sm-block" style="background-image: url(<?php the_field('banner_bg'); ?>)">

		
		<div class="container h-100">
            <div class="row h-100">     
                <div class="d-flex align-items-center h-100 w-100">
                    <div class="col-12 mt-4 mt-md-0">
                        <div class="row pt-md-5 mt-md-3" >
							<?php if( get_field('banner_heading') ): ?>
							<div class="col-12 ">
								 <h2 class="col-md-4 col-12 p-0 lh-43 d-flex align-items-center fc-white fw-100"><?php the_field('banner_heading'); ?></h2> 
							</div>
							

<?php endif; ?>
                               
							<?php if( get_field('banner_desc') ): ?>
                                <p class="col-md-3 fc-white position-relative fs-20 fc-blue walnut-desc"><?php the_field('banner_desc'); ?>
							<span><img src="<?php the_field('banner_desc_logo'); ?>" class="banner-desc-logo walnut-logo" /></span>
							</p>
							
							
	
<?php endif; ?>
                              
                                <div class="button-wrap pt-3 col-12">
									<?php if( get_field('banner_btn_text') ): ?>
                                        <a class="btn-apply white-text" href="<?php the_field('banner_btn_link'); ?>"><?php the_field('banner_btn_text'); ?></a>
									

<?php endif; ?>
                                    </div> 
                        </div>
                                 
                     </div>                        
                </div>      
           
            </div>
		</div>
	</section>
	
	<section class="w-100 banner banner-all web-bg d-block d-sm-none" style="background-image: url(<?php the_field('banner_mobile_bg'); ?>)">

		
		<div class="container h-100">
            <div class="row h-100">     
                <div class="d-flex align-items-center h-100 w-100">
                    <div class="col-12 mt-4 mt-md-0">
                        <div class="row pt-md-5 mt-md-3" >
							<?php if( get_field('banner_heading') ): ?>
							<div class="col-12 ">
								 <h2 class="col-md-4 col-12 p-0 lh-43 d-flex align-items-center fc-white fw-100"><?php the_field('banner_heading'); ?></h2> 
							</div>
							

<?php endif; ?>
                               
							<?php if( get_field('banner_desc') ): ?>
                                <p class="col-md-3 fc-white position-relative fs-20 fc-blue walnut-desc"><?php the_field('banner_desc'); ?>
							<span><img src="<?php the_field('banner_desc_logo'); ?>" class="banner-desc-logo walnut-logo" /></span>
							</p>
							
							
	
<?php endif; ?>
                              
                                <div class="button-wrap pt-3 col-12">
									<?php if( get_field('banner_btn_text') ): ?>
                                        <a class="btn-apply white-text" href="<?php the_field('banner_btn_link'); ?>"><?php the_field('banner_btn_text'); ?></a>
									

<?php endif; ?>
                                    </div> 
                        </div>
                                 
                     </div>                        
                </div>      
           
            </div>
		</div>
	</section>
	
	 <?php else: ?>
	
	<section class="w-100 banner banner-all d-none d-sm-block" style="background-image: url(<?php the_field('banner_bg'); ?>)">

		
		<div class="container h-100">
            <div class="row h-100">     
                <div class="d-flex align-items-center h-100 w-100">
                    <div class="col-12 mt-4 mt-md-0">
                        <div class="row pt-md-5 mt-md-3" >
							<?php if( get_field('banner_heading') ): ?>
							<div class="col-12 ">
								 <h2 class="col-md-5 col-12 p-0 lh-43 d-flex align-items-center fc-white text-uppercase fw-600"><?php the_field('banner_heading'); ?></h2> 
							</div>
							

<?php endif; ?>
                               
							<?php if( get_field('banner_desc') ): ?>
                                <p class="col-md-5 fc-brand"><?php the_field('banner_desc'); ?></p> 
							
	
<?php endif; ?>
                              
                                <div class="button-wrap pt-3 col-12">
									<?php if( get_field('banner_btn_text') ): ?>
                                        <a class="btn-apply white-text" href="<?php the_field('banner_btn_link'); ?>"><?php the_field('banner_btn_text'); ?></a>
									

<?php endif; ?>
                                    </div> 
                        </div>
                                 
                     </div>                        
                </div>      
           
            </div>
		</div>
	</section>
	
	<section class="w-100 banner banner-all d-block d-sm-none" style="background-image: url(<?php the_field('banner_mobile_bg'); ?>)">

		
		<div class="container h-100">
            <div class="row h-100">     
                <div class="d-flex align-items-center h-100 w-100">
                    <div class="col-12 mt-4 mt-md-0">
                        <div class="row pt-md-5 mt-md-3" >
							<?php if( get_field('banner_heading') ): ?>
							<div class="col-12 ">
								 <h2 class="col-md-5 col-12 p-0 lh-43 d-flex align-items-center fc-white text-uppercase fw-600"><?php the_field('banner_heading'); ?></h2> 
							</div>
							

<?php endif; ?>
                               
							<?php if( get_field('banner_desc') ): ?>
                                <p class="col-md-5 fc-brand"><?php the_field('banner_desc'); ?></p> 
							
	
<?php endif; ?>
                              
                                <div class="button-wrap pt-3 col-12">
									<?php if( get_field('banner_btn_text') ): ?>
                                        <a class="btn-apply white-text" href="<?php the_field('banner_btn_link'); ?>"><?php the_field('banner_btn_text'); ?></a>
									

<?php endif; ?>
                                    </div> 
                        </div>
                                 
                     </div>                        
                </div>      
           
            </div>
		</div>
	</section>

<?php endif ?>
<main class="page-content">
	
<!-- Modal -->
<div class="modal fade search-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
		  <a href="/home"><img src="/wp-content/themes/capitalfloat/images/logo-color.png" class="pl-4 mt-3"/></a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
             <?php //echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
		  <?php get_search_form();?>
      </div>
      <div class="modal-footer">
<!--         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
	
	
<!--<div class="loader"></div>-->