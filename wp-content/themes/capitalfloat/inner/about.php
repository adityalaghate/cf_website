<?php /* Template Name: about */ ?>
<?php get_header(); ?>


<main class="page-content">
<section class="w-100 py-md-5 py-3 position-relative">
    <div class="container">
        <div class="row">   
			<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-blue-h.png" alt="patern-image" class="about-wwr position-absolute d-none d-md-block">
            <div class="col-12">
                <h5 class="fw-600  pb-3 fc-slate2 text-uppercase"><?php the_field('section1_heading'); ?></h5>
            </div>
            <div class="col-md-4 col-12 pb-3 pb-sm-0">
                <img src="<?php the_field('section1_image'); ?>" alt="who-we-are" class="w-100"/>
            </div>
            <div class="col-md-6 col-12">
                <p class="pl-md-5 fc-slate2"><?php the_field('section1_description'); ?></p>
            </div>
        </div>
    </div>
</section>

<section class="w-100  position-relative">
    <div class="container">
        <div class="row">   
                <div class="solving-problem-bg-container d-none d-md-block">

                    </div>
            <div class="col-12 pl-2">
                <h5 class="fw-600 pl-1 pb-sm-3 pb-1 fc-slate2 text-uppercase pl-2"><?php the_field('section2_heading'); ?></h5>
            </div>
            <div class="col-md-6 col-12 d-flex align-items-center order-1 order-sm-0">
                <p class="fc-slate2 "><?php the_field('section2_description1'); ?></p>
            </div>
            <div class="col-md-4 col-12 offset-md-1 pb-3 pb-md-0 order-2 order-sm-0">
                <img src="<?php the_field('section2_image1'); ?>" alt="who-we-are" class="w-100"/>
            </div>    
            <div class="col-md-4 col-12 pt-md-5 pb-3 pb-md-0 order-4 order-sm-0">
                    <img src="<?php the_field('section2_image2'); ?>" alt="who-we-are" class="w-100"/>
                </div>   
            <div class="col-md-7 col-12 d-flex align-items-center pt-md-5 order-3 order-sm-0">
                    <p class="fc-slate2 px-md-5"><?php the_field('section2_description2'); ?></p>
            </div>
            <div class="col-md-6 col-12 d-flex align-items-center pt-md-5 order-5 order-sm-0">
                    <p class="fc-slate2 "><?php the_field('section2_description3'); ?></p>
                </div>
            <div class="col-md-4 col-12 offset-md-1 pt-md-5 order-6 order-sm-0">
                    <img src="<?php the_field('section2_image3'); ?>" alt="who-we-are" class="w-100"/>
            </div> 
            <div class="col-12 d-none">
                <p class="py-md-5 py-3 text-uppercase fw-800 fc-slate text-center"><?php the_field('section2_tagline'); ?></p>
            </div>                           
        </div>
    </div>
</section>

<section class="w-100 py-md-5 py-3 solving-problems position-relative">
    <div class="container py-3">
        <div class="row">  
			<img src="<?php echo get_template_directory_uri(); ?>/images/blue-pattern.PNG" alt="patern-image" class="about-ps position-absolute d-none d-md-block">
            <div class="col-12 text-center pb-md-5 pb-3">
                <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section3_heading'); ?></h5>
                <p class="w-75 m-auto px-md-5 fc-slate"><?php the_field('section3_description'); ?></p>
            </div>
            <div class="card col-md-4 col-12 pb-3 pb-md-0" style="width: 18rem;">
                <div class="card-inner  p-4 h-100">                
                    <img src="<?php the_field('sec3_card1_image'); ?>" class="pb-md-4 pb-3" alt="...">
                    <div class="card-body p-0">
                      <h6 class="card-title fc-slate fw-600 lh-24"><?php the_field('sec3_card1_heading'); ?></h6>
                      <p class="card-text fc-slate"><?php the_field('sec3_card1_description'); ?></p>
                    </div>
                </div>
            </div>
            <div class="card col-md-4 col-12 pb-3 pb-md-0" style="width: 18rem;">
                <div class="card-inner  p-4 h-100">
                    <img src="<?php the_field('sec3_card2_image'); ?>" class="pb-md-4 pb-3" alt="...">
                    <div class="card-body p-0">
                      <h6 class="card-title fc-slate fw-600 lh-24"><?php the_field('sec3_card2_heading'); ?></h6>
                      <p class="card-text fc-slate"><?php the_field('sec3_card2_description'); ?></p>
                    </div>
                </div>
            </div>
            <div class="card col-md-4 col-12 " style="width: 18rem;">
                <div class="card-inner  p-4 h-100">
                    <img src="<?php the_field('sec3_card3_image'); ?>" class="pb-md-4 pb-3" alt="...">
                    <div class="card-body p-0">
                      <h6 class="card-title fc-slate fw-600 lh-24"><?php the_field('sec3_card3_heading'); ?></h6>
                      <p class="card-text fc-slate"><?php the_field('sec3_card3_description'); ?></p>
                    </div>
                </div>
            </div>
                    
        </div>
    </div>
</section>

<section class="w-100 py-md-5 py-3  what-we-do">
        <div class="container">
            <div class="row">   
                <div class="col-12 pb-3 about-wwd">
                    <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section4_heading'); ?></h5>
                    <p class=" fc-slate"><?php the_field('section4_description'); ?></p>
                </div>
                <div class="card col-md-4 col-12 pb-3 pb-md-0" style="width: 18rem;">
                    <div class="card-inner">                
                        <img src="<?php the_field('sec4_card1_image'); ?>" class="pb-md-3 pb-3 w-100" alt="...">
                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec4_card1_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_field('sec4_card1_description'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-4 col-12 pb-3 pb-md-0" style="width: 18rem;">
                    <div class="card-inner">
                        <img src="<?php the_field('sec4_card2_image'); ?>" class="pb-md-3 pb-3 w-100" alt="...">
                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec4_card2_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_field('sec4_card2_description'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-4 col-12 pb-3 pb-md-0" style="width: 18rem;">
                    <div class="card-inner">
                        <img src="<?php the_field('sec4_card3_image'); ?>" class="pb-md-3 pb-3 w-100" alt="...">
                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec4_card3_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_field('sec4_card3_description'); ?></p>
                        </div>
                    </div>
                </div>
                        
            </div>
        </div>
</section>


	 
	<section class="w-100 pt-md-5 pt-3 our-history fs-13 position-relative">
		<div class="container">
			<div class="row">
<!-- 				<img src="http://13.232.227.71/wp-content/themes/capitalfloat/images/pattern-right.png" alt="patern-image" class="c-float-pattern position-absolute d-none d-md-block"> -->
				
				<div class="col-12 about-oh">
                    <h5 class="fw-600  fc-slate2 text-uppercase"><?php the_field('section5_heading'); ?></h5>
                    <p class=" fc-slate"><?php the_field('section5_description'); ?></p>
                </div>
				
				<div class="col-12">
					
					<?php if( have_rows('tabs') ): ?>
	
	<div class="tab-content pb-md-4 pb-3">
		<?php $i=0; while ( have_rows('tabs') ) : the_row(); ?>
			<?php 
				$string = sanitize_title( get_sub_field('year_title') ); 
			?>
		    <div role="tabpanel" class="tab-pane fade <?php if ($i==0) { ?>in active show<?php } ?>" id="<?php echo $string; ?>">
				<div class="row">
					<div class="col-md-6">
                    <p class=" fc-slate"><?php the_sub_field('year_desc'); ?></p>
						
						
					</div>
					
					
					<div class="col-md-8 col-12 pt-md-3">
						<div class="row">
							<div class="col-6 mb-4">
								<div class="card text-center pt-3 pt-md-4 h-100">
								  <img src="<?php the_sub_field('our_history_card1_img'); ?>" class="card-img-top mx-auto" alt="...">
								  <div class="card-body p-1 p-md-2">
									<h5 class="card-title font-weight-bold">Lifetime Loan Disbursal</h5>
									<p class="card-text fs-20 fw-600"><?php the_sub_field('lld_counts'); ?></p>							
								  </div>
								</div>								
							</div>
							<div class="col-6 mb-4">
								<div class="card text-center pt-4 h-100">
								  <img src="<?php the_sub_field('our_history_card2_img'); ?>" class="card-img-top mx-auto" alt="...">
								  <div class="card-body p-1 p-md-2">
									<h5 class="card-title font-weight-bold">Customers</h5>
									<p class="card-text fs-20 fw-600"><?php the_sub_field('customers_counts'); ?></p>							
								  </div>
								</div>								
							</div>
							<div class="col-6">
								<div class="card text-center pt-4 h-100">
								  <img src="<?php the_sub_field('our_history_card3_img'); ?>" class="card-img-top mx-auto" alt="...">
								  <div class="card-body p-1 p-md-2">
									<h5 class="card-title font-weight-bold">Cities</h5>
									<p class="card-text fs-20 fw-600"><?php the_sub_field('cities_counts'); ?></p>							
								  </div>
								</div>								
							</div>
							<div class="col-6">
								<div class="card text-center pt-4 h-100">
								  <img src="<?php the_sub_field('our_history_card4_img'); ?>" class="card-img-top mx-auto" alt="...">
								  <div class="card-body p-1 p-md-2">
									<h5 class="card-title font-weight-bold">Employees</h5>
									<p class="card-text fs-20 fw-600"><?php the_sub_field('employees_counts'); ?></p>						
								  </div>
								</div>								
							</div>
							
						</div>
					</div>
					<div class="col-md-4 col-12 text-right d-flex justify-content-end oh-tab-right-content">
						<div class="bg-div oh-rhs">
							<img src="<?php the_sub_field('rhs_image'); ?>" class="w-100 mb-3"/>
							<h4 class=" mb-1"><?php the_sub_field('rhs_text'); ?></h4>
							<p class="font-weight-bold"><?php the_sub_field('rhs_count'); ?></p>
						</div>
						
					</div> 		 
					
					
					
					<!-- 					<div class="col-md-5 col-12 pt-md-3">
						<p class="fc-slate fs-14 fw-600 ls-08"><?php the_sub_field('year'); ?></p>
						<h6 class="fc-slate fw-600 ls-08 pl-3 position-relative"><?php the_sub_field('title'); ?></h6>
						<p class="fc-slate pt-md-4 pt-3"><?php the_sub_field('year_description'); ?></p>
					</div>
					<div class="col-md-3 offset-md-1 col-12">
						<div class="bg-div d-none d-md-block">
						</div>
					</div> 		 --> 
					
				</div>

				
		    </div>
		<?php $i++; endwhile; ?>
	</div>
					
					<ul class=" nav-tabs p-0  w-100 years py-3 " id="myTab" role="tablist">
		<?php $i=0; while ( have_rows('tabs') ) : the_row(); ?>
			<?php 
				$string = sanitize_title( get_sub_field('year_title') ); 
			?>
			<li role="presentation" <?php if ($i==0) { ?>class="active show"<?php } ?>  >
				<a class="text-li <?php if ($i==0) { ?>in active show<?php } ?>" href="#<?php echo $string ?>" aria-controls="<?php echo $string ?>" role="tab" data-toggle="tab"><img src="<?php the_sub_field('km-milestone'); ?>" class="our-history-tab-img"/></a>
			</li>
		<?php $i++; endwhile; ?>
	</ul>
					
<?php endif; ?>
					
					
				</div>				
			</div>			
		</div>	
	</section>
	
	
	

<section class="w-100 our-partners py-md-5 py-3">
    <div class="container">
        <div class="row">   
            <div class="col-12">
                <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase text-center"><?php the_field('section6_heading'); ?></h5>
            </div>
            <div class="rio-promos-about col-12 mb-0 mt-md-4">
				
				<?php if( have_rows('logo') ): ?>
				<?php while( have_rows('logo') ): the_row();?>
				<div class="col">
                <img src="<?php the_sub_field('logo_image'); ?>" class="w-100"/>
					
					
				</div>


				<?php endwhile; ?>
				<?php endif; ?>

            </div>
        </div>
    </div>
</section>


  </main>
<?php get_footer(); ?>
