<?php /* Template Name: products */ ?>
<?php get_header(); ?>
        
<main>
<?php if ( is_page('term-finance') || is_page('loans-against-card-swipes') || is_page('school-finance') || is_page('doctor-loans') || is_page('business-loans-in-bangalore') || is_page('business-loans-in-pune') || is_page('business-loans-in-chennai') || is_page('business-loans-in-hyderabad') || is_page('business-loans-in-mumbai') || is_page('business-loans-in-delhi') || is_page('business-loan-eligibility')  || is_page('business-loan-emi-calculator')  || is_page('commercial-loans')  || is_page('documents-required-for-a-business-loan') || is_page('business-loan-for-women') || is_page('working-capital-loans') || is_page('sme-msme-loans')  || is_page('business-loan-interest-rates')): ?>

	   <div class="container product-tab p-0 d-none d-md-block fixedElement" id="product-tab">            
              <div class="scroller scroller-left mt-1"><i class="fa fa-chevron-left"></i></div>
              <div class="scroller scroller-right mt-1"><i class="fa fa-chevron-right"></i></div>
              <div class="wrapper">
               <nav class="nav nav-tabs list" id="myTab" role="tablist">
              <a class="nav-item nav-link desc-tab active px-md-5 px-3 py-3" id="public-chat-tab" href="#product-desc">
				  <img class="dti" src="/wp-content/uploads/2019/10/Description.png" />

				  <?php the_field('sec1_nav1'); ?></a>
              <a class="nav-item nav-link hiw-tab px-md-5 px-3 py-3" href="#product-hiw">
				  <img class="hiwti" src="/wp-content/uploads/2019/10/Process.png" />

				  <?php the_field('sec1_nav2'); ?></a>
              <a class="nav-item nav-link benefits-tab px-md-5 px-3 py-3" href="#product-benefits">
				  <img class="bti" src="/wp-content/uploads/2019/10/Benefits.png" />

				  <?php the_field('sec1_nav3'); ?></a>
              <a class="nav-item nav-link testimonials-tab px-md-5 px-3 py-3" href="#products-testimonials" role="tab" data-toggle="tab">
				  <img class="tti" src="/wp-content/uploads/2019/10/Testimonials.png" />

				  <?php the_field('sec1_nav4'); ?></a>
              <a class="nav-item nav-link ir-tab px-md-5 px-3 py-3" href="#product-irf" role="tab" data-toggle="tab">
				  <img class="irti" src="/wp-content/uploads/2019/10/Interest_Rate.png" />

				  <?php the_field('sec1_nav5'); ?></a>
              <a class="nav-item nav-link emi-tab px-md-5 px-3 py-3" href="#product-ec" role="tab" data-toggle="tab">
				  <img class="emiti" src="/wp-content/uploads/2019/10/EMI_Calculator.png" />

				  <?php the_field('sec1_nav6'); ?></a>
              <a class="nav-item nav-link eligibility-tab px-md-5 px-3 py-3" href="#product-eligibility" role="tab" data-toggle="tab">
				  <img class="eti" src="/wp-content/uploads/2019/10/Eligibility.png" />

				  <?php the_field('sec1_nav7'); ?></a>
              <a class="nav-item nav-link faq-tab px-md-5 px-3 py-3" href="#product-faq" role="tab" data-toggle="tab">
				  <img class="faqti" src="/wp-content/uploads/2019/10/FAQ.png" />

				  <?php the_field('sec1_nav8'); ?></a>
              <a class="nav-item nav-link sp-tab px-md-5 px-3 py-3" href="#product-sp" role="tab" data-toggle="tab">
				  <img class="spti" src="/wp-content/uploads/2019/10/Sub_Products.png" />

				  <?php the_field('sec1_nav9'); ?></a>
              <a class="nav-item nav-link li-tab px-md-5 px-3 py-3" href="#product-li" role="tab" data-toggle="tab">
				  <img class="liti" src="/wp-content/uploads/2019/10/explore-1.png" />

				  <?php the_field('sec1_nav10'); ?></a>                    
            </nav>
              </div>              
            </div>
	
	
<!----------------------------------------- Mobile Product Tab starts here --------------------------------------------->
	

	
	<div class="input-group prod-tab-mobile d-md-none d-block">
  
  <select class="custom-select" id="prod-tab-mobile">
    <option value="#product-desc" selected >Choose...</option>
    <option value="#product-desc"><?php the_field('sec1_nav1'); ?></option>
    <option value="#product-hiw"><?php the_field('sec1_nav2'); ?></option>
    <option value="#product-benefits"><?php the_field('sec1_nav3'); ?></option>
    <option value="#products-testimonials"><?php the_field('sec1_nav4'); ?></option>
    <option value="#product-irf"><?php the_field('sec1_nav5'); ?></option>
    <option value="#product-ec"><?php the_field('sec1_nav6'); ?></option>
    <option value="#product-eligibility"><?php the_field('sec1_nav7'); ?></option>
    <option value="#product-faq"><?php the_field('sec1_nav8'); ?></option>
    <option value="#product-sp"><?php the_field('sec1_nav9'); ?></option>
    <option value="#product-li"><?php the_field('sec1_nav10'); ?></option>
    
	  
  </select>
</div>
	
<!----------------------------------------- Mobile Product Tab Ends here --------------------------------------------->
    
           
<section class="w-100 py-md-5 py-3 description position-relative" id="product-desc">
	<div class="container">
		<div class="row">
			<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-h.png" alt="patern-image" class="pdt-m position-absolute d-none d-md-block">
			<div class="col-md-5 offset-md-1 left-content pr-md-5 py-3 fc-slate2">
                <h5 class="position-relative fw-600"><?php the_field('section2_heading'); ?></h5>              
                <p class="fs-14"><?php the_field('section2_desc'); ?></p>
            </div>
            <div class="col-md-6 col-12 right-content">		
                    <div class="card mb-md-4 mb-3">
                        <div class="row no-gutters d-flex justify-content-md-center">
                            <div class="col-1 offset-lg-1">
                            <img src="<?php the_field('sec2_card1_image'); ?>" class="card-img" alt="...">
                            </div>
                            <div class="col-md-6 col-8 col-lg-5">
                            <div class="card-body py-0">
                                <h6 class="card-title mb-0"><?php the_field('sec2_card1_heading'); ?></h6>
                                <p class="card-text"><small class="text-muted"><?php the_field('sec2_card1_desc'); ?></small></p>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-md-4 mb-3">
                        <div class="row no-gutters d-flex justify-content-md-center">
                            <div class="col-1 offset-lg-1">
                            <img src="<?php the_field('sec2_card2_image'); ?>" class="card-img" alt="...">
                            </div>
                            <div class="col-md-6 col-8 col-lg-5">
                            <div class="card-body py-0">
                                <h6 class="card-title mb-0"><?php the_field('sec2_card2_heading'); ?></h6>
                                <p class="card-text"><small class="text-muted"><?php the_field('sec2_card2_desc'); ?></small></p>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-md-4 mb-3">
                        <div class="row no-gutters d-flex justify-content-md-center">
                            <div class="col-1 offset-lg-1">
                            <img src="<?php the_field('sec2_card3_image'); ?>" class="card-img" alt="...">
                            </div>
                            <div class="col-md-6 col-8 col-lg-5">
                            <div class="card-body py-0">
                                <h6 class="card-title mb-0"><?php the_field('sec2_card3_heading'); ?></h6>
                                <p class="card-text"><small class="text-muted"><?php the_field('sec2_card3_desc'); ?></small></p>
                            </div>
                            </div>
                        </div>
                    </div>	
            </div>
		</div>
	</div>
</section>

<?php elseif ( is_page('online-checkout-finance') ): ?>

<section class="w-100 py-md-5 py-3 position-relative">
    <div class="container">
        <div class="row">   
			<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-blue-h.png" alt="patern-image" class="about-wwr position-absolute d-none d-md-block">
            <div class="col-12">
                <h5 class="fw-600  pb-3 fc-slate2 text-uppercase">WHAT IS ONLINE CHECKOUT FINANCE</h5>
            </div>
            <div class="col-md-4 col-12 pb-3 pb-sm-0">
                <img src="/wp-content/uploads/2019/09/What_Is_OCF-min.jpg" alt="Online Checkout Finance" class="w-100"/>
            </div>
            <div class="col-md-6 col-12">
                <p class="pl-md-5 fc-slate2">With Capital Float’s Online Checkout Finance, customers can select our instant zero cost/low-cost EMI option during checkout to purchase products without making the full payment upfront. This credit solution enables customers to break-down the cost of the purchased item into multiple EMIs, thereby increasing the affordability of the purchase. </p>

                <p class="pl-md-5 fc-slate2">We've partnered with leading e-commerce websites such as Amazon to offer Online Checkout Finance to customers across India. Customers can also benefit from features such as auto-repayments, tracking EMI history and pending dues, that make the credit solution very transparent and easy to use.</p>
            </div>
        </div>
    </div>
</section>

<?php elseif ( is_page('merchant-checkout-finance') ): ?>

<section class="w-100 py-md-5 py-3 position-relative">
    <div class="container">
        <div class="row">   
			<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-blue-h.png" alt="patern-image" class="about-wwr position-absolute d-none d-md-block">
            <div class="col-12">
                <h5 class="fw-600  pb-3 fc-slate2 text-uppercase">WHAT IS MERCHANT CHECKOUT FINANCE</h5>
            </div>
            <div class="col-md-4 col-12 pb-3 pb-sm-0">
                <img src="/wp-content/uploads/2019/09/What_Is_MCF.jpg" alt="Online Checkout Finance" class="w-100"/>
            </div>
            <div class="col-md-6 col-12">
                <p class="pl-md-5 fc-slate2">Merchant Checkout Finance makes expensive purchases affordable for consumers by quickly converting the overall expenditure into easy installments. This cutting-edge financial solution empowers consumers to acquire a desired product or avail an aspirational service without any delay. </p>

                <p class="pl-md-5 fc-slate2">Capital Float has partnered with leading brands across the country to provide consumers with the flexibility to shop without having to worry about their present cash flow. This highly customized financing offering gives consumers the option to opt for an affordable repayment plan, therefore increasing the purchasing power of the borrower.</p>
            </div>
        </div>
    </div>
</section>

	
<?php elseif ( is_page('mca-sub-page-petrolpump') || is_page('mca-sub-page-restaurant') ): ?>
	
	
	<section class="w-100 py-md-5 py-3 description position-relative" id="product-desc">
	<div class="container">
		<div class="row">
			<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-h.png" alt="patern-image" class="pdt-m position-absolute d-none d-md-block">
			<div class="col-md-5 offset-md-1 left-content pr-md-5 py-3 fc-slate2">
                <h5 class="position-relative fw-600"><?php the_field('section2_heading'); ?></h5>              
                <p class="fs-14"><?php the_field('section2_desc'); ?></p>
            </div>
            <div class="col-md-6 col-12 right-content">		
                    <div class="card mb-md-4 mb-3">
                        <div class="row no-gutters d-flex justify-content-md-center">
                            <div class="col-1 offset-lg-1">
                            <img src="<?php the_field('sec2_card1_image'); ?>" class="card-img" alt="...">
                            </div>
                            <div class="col-md-6 col-8 col-lg-5">
                            <div class="card-body py-0">
                                <h6 class="card-title mb-0"><?php the_field('sec2_card1_heading'); ?></h6>
                                <p class="card-text"><small class="text-muted"><?php the_field('sec2_card1_desc'); ?></small></p>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-md-4 mb-3">
                        <div class="row no-gutters d-flex justify-content-md-center">
                            <div class="col-1 offset-lg-1">
                            <img src="<?php the_field('sec2_card2_image'); ?>" class="card-img" alt="...">
                            </div>
                            <div class="col-md-6 col-8 col-lg-5">
                            <div class="card-body py-0">
                                <h6 class="card-title mb-0"><?php the_field('sec2_card2_heading'); ?></h6>
                                <p class="card-text"><small class="text-muted"><?php the_field('sec2_card2_desc'); ?></small></p>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-md-4 mb-3">
                        <div class="row no-gutters d-flex justify-content-md-center">
                            <div class="col-1 offset-lg-1">
                            <img src="<?php the_field('sec2_card3_image'); ?>" class="card-img" alt="...">
                            </div>
                            <div class="col-md-6 col-8 col-lg-5">
                            <div class="card-body py-0">
                                <h6 class="card-title mb-0"><?php the_field('sec2_card3_heading'); ?></h6>
                                <p class="card-text"><small class="text-muted"><?php the_field('sec2_card3_desc'); ?></small></p>
                            </div>
                            </div>
                        </div>
                    </div>	
            </div>
		</div>
	</div>
</section>
	
	
	
<?php else: ?>
	

<section class="w-100 py-md-5 py-3 position-relative test-disha">
    <div class="container">
        <div class="row">   
			<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-blue-h.png" alt="patern-image" class="about-wwr position-absolute d-none d-md-block">
            <div class="col-12">
                <h5 class="fw-600  pb-3 fc-slate2 text-uppercase"><?php the_field('section2_heading'); ?></h5>
            </div>
            <div class="col-md-4 col-12 pb-3 pb-sm-0">
                <img src="/wp-content/uploads/2019/09/What_Is_Walnut.jpg" alt="Online Checkout Finance" class="w-100"/>
            </div>
            <div class="col-md-6 col-12">
                <p class="pl-md-5 fc-slate2"><?php the_field('section2_desc'); ?></p>

               
            </div>
        </div>
    </div>
</section>

<?php endif; ?>

	
<section class="w-100 how-it-works-product py-md-5 py-3" id="product-hiw" style="background-image: url(<?php the_field('section3_bg'); ?>)">
    <div class="container py-3">
        <div class="row">
            <div class="col-md-12">
            <h5 class="text-uppercase fc-slate fw-600 text-center pb-md-4 pb-3"><?php the_field('section3_heading'); ?></h5>
            </div>
			
			<?php if ( is_page('walnut') || is_page('merchant-checkout-finance') ): ?>
			
			<?php if( have_rows('section3_card') ): ?>
	<?php while( have_rows('section3_card') ): the_row();?>
			
            <div class="col-md-3 mb-4 repeater-product d-none d-md-block">
                <div class="card p-4 h-100">                  
                    <img src="<?php the_sub_field('sec3_card_image'); ?>" class="card-img-top" alt="...">
                    <div class="card-body p-0 pt-3">
                        <h6 class="card-title fc-slate text-uppercase fw-600 mb-2"><?php the_sub_field('sec3_card_heading'); ?></h6>
                        <p class="card-text m-0 fc-slate"><?php the_sub_field('sec3_card_desc'); ?></p>
                    </div>                    
                </div>            
            </div>
			
			<?php endwhile; ?>
<?php endif; ?>
			
			<?php else: ?>
			
				<?php if( have_rows('section3_card') ): ?>
	<?php while( have_rows('section3_card') ): the_row();?>
			
            <div class="col-md-4 mb-4 repeater-product d-none d-md-block">
                <div class="card p-4 h-100">                  
                    <img src="<?php the_sub_field('sec3_card_image'); ?>" class="card-img-top" alt="...">
                    <div class="card-body p-0 pt-3">
                        <h6 class="card-title fc-slate text-uppercase fw-600 mb-2"><?php the_sub_field('sec3_card_heading'); ?></h6>
                        <p class="card-text m-0 fc-slate"><?php the_sub_field('sec3_card_desc'); ?></p>
                    </div>                    
                </div>            
            </div>
			
			<?php endwhile; ?>
<?php endif; ?>
			
			<?php endif ?>
			
   
<!------------------------------------------- Mobile view Starts here ------------------------------------->
			
            <div class="rio-promos-product mv-content d-block d-md-none col-12 ">  
				
					<?php if( have_rows('section3_card') ): ?>
	<?php while( have_rows('section3_card') ): the_row();?>
			
            <div class="col-md-3 mb-4 repeater-product p-0" style="width:18rem;">
                <div class="card p-4">                  
                    <img src="<?php the_sub_field('sec3_card_image'); ?>" class="card-img-top" alt="...">
                    <div class="card-body p-0 pt-3">
                        <h6 class="card-title fc-slate text-uppercase fw-600 mb-2"><?php the_sub_field('sec3_card_heading'); ?></h6>
                        <p class="card-text m-0 fc-slate"><?php the_sub_field('sec3_card_desc'); ?></p>
                    </div>                    
                </div>            
            </div>
			
			<?php endwhile; ?>
<?php endif; ?>
						
						
			</div>
			
<!------------------------------------------- Mobile view Ends here ------------------------------------->
                
        </div>
    </div>					
</section>

<section class="w-100 py-md-5 py-3 benefits-product position-relative " id="product-benefits">
    <div class="container">
        <div class="row">
            <div class="col-md-5 offset-md-1 left-content pr-md-5 py-3 fc-slate2">
                <h5 class="position-relative fw-600 text-uppercase mb-3"><?php the_field('section4_heading'); ?></h5>              
                <p class="fs-14"><?php the_field('section4_desc'); ?></p>
                <ul class="pl-3">
					
					<?php if( have_rows('section4_benefits_points') ): ?>
					<?php while( have_rows('section4_benefits_points') ): the_row();?>
					
                    <li class="mb-3 fs-14"><?php the_sub_field('benefits_point'); ?></li>
					
					<?php endwhile; ?>
					<?php endif; ?>
                           
                </ul>
            </div>
			
			
            <div class="col-md-5 col-12 right-content py-md-3 ">	  
				<img src="<?php the_field('benefits_image'); ?>" class="w-100" />
                
            </div>
        </div>
    </div>
</section>



 <section class="w-100 media py-md-5 py-2 position-relative my-md-5 my-3 " id="products-testimonials">
            <div class="container">
                <div class="row">   
			<div class="pted position-absolute d-none d-md-block"></div>
					
							<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-blue-h.png" alt="patern-image" class="ce-m position-absolute d-none d-md-block">
						<img src="<?php echo get_template_directory_uri(); ?>/images/blue-pattern.PNG" alt="patern-image" class="ce-m1 position-absolute d-none d-md-block">
				
                    <div class="col-12">
                    <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase text-center"><?php the_field('section5_heading'); ?></h5>
                    </div>
                    <div class="rio-promos-product-testimonials col-12 col-md-10 mx-auto">    
						
						<?php if( have_rows('section5_card') ): ?>

	<?php while( have_rows('section5_card') ): the_row();?>
		
                        <div class="card col-md-12 text-center py-5 px-md-4 px-0" style="width: 18rem;">
                            <div class="position-relative img-div"> 
                                <img src="<?php the_sub_field('sec5_card_image'); ?>" alt="testimonials-image" class="card-img-top mx-auto pb-3">
                            </div>
                            <div class="card-body px-md-2 px-4">    
                                <h6 class="card-title fc-blue mb-1"><?php the_sub_field('sec5_card_name'); ?></h6>   
                                <span class="text-uppercase fc-slate"><small><?php the_sub_field('sec5_card_role'); ?></small></span>
								 <p class="text-uppercase fc-slate m-0"><small><?php the_sub_field('sec5_card_company'); ?></small></p>
                                <p class="fc-slate quote-text position-relative pt-5 fs-13"><?php the_sub_field('sec5_card_desc'); ?></p>
                            </div>
                        </div>
                   <?php endwhile; ?>

<?php endif; ?>
                
                    </div> 
                </div>
            </div>
        </section> 


<?php if ( is_page('term-finance') || is_page('loans-against-card-swipes') || is_page('school-finance') || is_page('doctor-loans') || is_page('business-loans-in-bangalore') || is_page('business-loans-in-pune') || is_page('business-loans-in-hyderabad') || is_page('business-loans-in-mumbai') || is_page('business-loans-in-chennai') || is_page('business-loans-in-delhi') || is_page('mca-sub-page-petrolpump') || is_page('mca-sub-page-restaurant') || is_page('business-loan-eligibility')  || is_page('business-loan-emi-calculator')  || is_page('commercial-loans')  || is_page('documents-required-for-a-business-loan') || is_page('business-loan-for-women') || is_page('working-capital-loans')   || is_page('business-loan-interest-rates')|| is_page('sme-msme-loans')): ?>


<section class="w-100 pt-md-4 pt-2 pb-md-5 pb-3 product-emi-calculator" id="product-irf">
        <div class="container">
            <div class="row">
                <div class="col-md-5 offset-md-1 left-content pr-md-5 py-3 fc-slate2">
                    <h5 class="position-relative"><?php the_field('section6_heading'); ?></h5>
                    <hr>
                    <p class="fs-14"><?php the_field('section6_desc'); ?></p>
                </div>
                <div class="col-md-6 col-12 right-content">				
                    <div class="row fs-15">
                        <div class="col-6 col-md-5 content mb-3 mb-md-0">
                            <div class="mb-3 h-100 d-flex align-items-center">
								<div class="w-100">							
									<img src="<?php the_field('sec6_card1_img'); ?>" class="d-block mx-auto mb-3" />
									<p class="fs-12 ls-08 m-0 px-3 fc-slate"><?php the_field('sec6_card1_text'); ?></p>
									<span class="flat">* Flat</span>
								</div>
                            </div>
                        </div>
                        <div class="col-6 col-md-5 content pl-md-0 mb-3 mb-md-0">
                            <div class="mb-3 h-100 d-flex align-items-center">
								<div class=w-100>								
                                    <img src="<?php the_field('sec6_card2_img'); ?>" class="d-block mx-auto mb-3" />
                                    <p class="fs-12 ls-08 m-0 px-3 fc-slate"><?php the_field('sec6_card2_text'); ?></p>
										
								</div>
                            </div>
                        </div>
                        <div class="col-6 col-md-5 content">
                            <div class="mb-3 h-100 mt-md-3 d-flex align-items-center">
								<div class="w-100">
									
								
                                    <img src="<?php the_field('sec6_card3_img'); ?>" class="d-block mx-auto mb-3" />
                                    <p class="fs-12 ls-08 m-0 px-3 fc-slate"><?php the_field('sec6_card3_text'); ?></p>
									</div>
                            </div>
                        </div>
                        <div class="col-6 col-md-5 content pl-md-0">
                            <div class="mb-3 h-100 mt-md-3 d-flex align-items-center">
								<div class="w-100">
									
								

                                    <img src="<?php the_field('sec6_card4_img'); ?>" class="d-block mx-auto mb-3" />
                                    <p class="fs-12 ls-08 m-0 px-3 fc-slate"><?php the_field('sec6_card4_text'); ?></p>
																	</div>
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
</section>

	
	<?php if ( is_page('loans-against-card-swipes') || is_page('mca-sub-page-petrolpump') || is_page('mca-sub-page-restaurant') ): ?>
	
	
<section class="w-100 pt-md-2 pb-md-5 pb-3 lpr" id="product-ec">
        <div class="container">
            <div class="row py-3">
                <div class="col-12">
                        <h5 class="position-relative fc-slate2 fw-600"><?php the_field('section7_heading'); ?></h5> 
                </div>
            </div>
            <div class="row mx-0 slider-bg py-md-5 py-3">
                <div class="col-md-6 col-12 offset-md-1 left-content pr-4 pr-md-5 fc-slate2 mb-md-0 mb-4">
                        <div class="row">
                            <div class="col-sm-12">
								<div class="pb-4 mb-2">
									<p class="fw-600 fc-slate float-left">Loan Amount</p>
								    <input id="amount" class="w-50 float-right border-0 bg-transparent text-right" type="text" disabled value="12" />
								</div>
                                
                            <div id="slider-range"></div>
                            </div>
                        </div>
                        <div class="row slider-labels pb-4">
                            <div class="col-6 caption">
                            <strong>Min:</strong> <span id="slider-range-value1"></span>
                            </div>
                            <div class="col-6 text-right caption">
                            <strong>Max:</strong> <span id="slider-range-value2"></span>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-12">
                             <div class="pb-4 mb-2">
									<p class="fw-600 fc-slate float-left">Tenure (months)</p>
								    <input id="month" class="w-25 float-right border-0 bg-transparent text-right" type="text" disabled value="12" />
								</div>
                                <div id="slider-range1"></div>
                                </div>
                            </div>
                            <div class="row slider-labels pb-4">
                                <div class="col-6 caption">
                                <strong>Min:</strong> <span id="slider-range1-value1"></span>
                                </div>
                                <div class="col-6 text-right caption">
                                <strong>Max:</strong> <span id="slider-range1-value2"></span>
                                </div>
                            </div>
                
                        <div class="row">
                                <div class="col-sm-12">
                                <div class="pb-4 mb-2">
									<p class="fw-600 fc-slate float-left">Interest Rate (%)</p>
								    <input id="rate" class="w-25 float-right border-0 bg-transparent text-right" type="text" disabled value="12" />
								</div>

                                <div id="slider-range2"></div>
                                </div>
                            </div>
                            <div class="row slider-labels pb-4">
                                <div class="col-6 caption">
                                <strong>Min:</strong> <span id="slider-range2-value1"></span>
                                </div>
                                <div class="col-6 text-right caption">
                                <strong>Max:</strong> <span id="slider-range2-value2"></span>
                                </div>
                            </div>
                  

                        <div class="button-wrap pt-3">
                               <a href="https://safe.capitalfloat.com/cf/default/register" target="_new"><button class="btn-apply fs-12 mb-3 mb-md-0"><?php the_field('section7_apply_now_btn_text'); ?></button></a>
                               
                            </div>
                                       
                </div>

                <div class="col-md-3 col-12 right-content offset-md-1">				
                    

<div class="calc-amount text-center p-5 mt-md-5">
    <p class="fs-20 fc-white text-uppercase mb-0">Your EMI</p>
    <p class="fc-white results" id="result1-l">₹ 0</p>
</div>
               </div>
            </div>
        </div>
</section>
	
	<?php else: ?>
	
	<section class="w-100 pt-md-2 pb-md-5 pb-3 " id="product-ec">
        <div class="container">
            <div class="row py-3">
                <div class="col-12">
                        <h5 class="position-relative fc-slate2 fw-600"><?php the_field('section7_heading'); ?></h5> 
                </div>
            </div>
            <div class="row mx-0 slider-bg py-md-5 py-3">
                <div class="col-md-6 col-12 offset-md-1 left-content pr-4 pr-md-5 fc-slate2 mb-md-0 mb-4">
                        <div class="row">
                            <div class="col-sm-12">
								<div class="pb-4 mb-2">
									<p class="fw-600 fc-slate float-left">Loan Amount</p>
								    <input id="amount" class="w-50 float-right border-0 bg-transparent text-right" type="text" disabled value="12" />
								</div>
                                
                            <div id="slider-range"></div>
                            </div>
                        </div>
                        <div class="row slider-labels pb-4">
                            <div class="col-6 caption">
                            <strong>Min:</strong> <span id="slider-range-value1"></span>
                            </div>
                            <div class="col-6 text-right caption">
                            <strong>Max:</strong> <span id="slider-range-value2"></span>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-12">
                             <div class="pb-4 mb-2">
									<p class="fw-600 fc-slate float-left">Tenure (months)</p>
								    <input id="month" class="w-25 float-right border-0 bg-transparent text-right" type="text" disabled value="12" />
								</div>
                                <div id="slider-range1"></div>
                                </div>
                            </div>
                            <div class="row slider-labels pb-4">
                                <div class="col-6 caption">
                                <strong>Min:</strong> <span id="slider-range1-value1"></span>
                                </div>
                                <div class="col-6 text-right caption">
                                <strong>Max:</strong> <span id="slider-range1-value2"></span>
                                </div>
                            </div>
                
                        <div class="row">
                                <div class="col-sm-12">
                                <div class="pb-4 mb-2">
									<p class="fw-600 fc-slate float-left">Interest Rate (%)</p>
								    <input id="rate" class="w-25 float-right border-0 bg-transparent text-right" type="text" disabled value="12" />
								</div>

                                <div id="slider-range2"></div>
                                </div>
                            </div>
                            <div class="row slider-labels pb-4">
                                <div class="col-6 caption">
                                <strong>Min:</strong> <span id="slider-range2-value1"></span>
                                </div>
                                <div class="col-6 text-right caption">
                                <strong>Max:</strong> <span id="slider-range2-value2"></span>
                                </div>
                            </div>
                  

                        <div class="button-wrap pt-3">
                                <button class="btn-apply fs-12 mb-3 mb-md-0"><?php the_field('section7_apply_now_btn_text'); ?></button>
                                <button class="btn-cancel fs-12 request-cb d-none d-md-inline-block"><?php the_field('section7_apply_request_btn_text'); ?></button>
                            </div>
                                       
                </div>

                <div class="col-md-3 col-12 right-content offset-md-1">				
                    

<div class="calc-amount text-center p-5 mt-md-5">
    <p class="fs-20 fc-white text-uppercase mb-0">Your EMI</p>
    <p class="fc-white results" id="result1">₹ 0</p>

</div>
               </div>
            </div>
        </div>
</section>
	
	
<?php endif; ?>

<?php else: ?>
<?php endif; ?>

<?php if ( is_page('doctor-loans') ): ?>


<section class="w-100 pb-md-3 eligibility doctor-eligibility " id="product-eligibility">
        <div class="container py-3">
            <div class="row">  
                <div class="col-12 text-center pt-md-5 pt-3">
                    <h5 class="fw-600 pl-1  fc-slate2 text-uppercase"><?php the_field('section8_heading'); ?></h5>
                    <h6>For Self-Employed Doctors</h6>
                </div>
                <div class="card col-md-4 col-12 text-center" style="width: 18rem;">
                    <div class="card-inner  p-4">                
                        <img src="<?php the_field('sec8_card1_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="eligibility-bg-div"></div>
                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec8_card1_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_field('sec8_card1_desc'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-4 col-12 text-center" style="width: 18rem;">
                    <div class="card-inner  p-4">
                        <img src="<?php the_field('sec8_card2_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="eligibility-bg-div"></div>


                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec8_card2_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_field('sec8_card2_desc'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-4 col-12 text-center" style="width: 18rem;">
                    <div class="card-inner  p-4">
                        <img src="<?php the_field('sec8_card3_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="eligibility-bg-div"></div>

                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec8_card3_heading'); ?></h6>
                          <p class="card-text fc-slate px-4"><?php the_field('sec8_card3_desc'); ?></p>
                        </div>
                    </div>
                </div>
             
                        
            </div>
            <div class="row">  
                <div class="col-12 text-center pt-md-5 pt-3">
                    <h6>For Consulting Doctors</h6>
                </div>
                <div class="card col-md-4 col-12 text-center" style="width: 18rem;">
                    <div class="card-inner  p-4">                
                        <img src="<?php the_field('sec8_card1_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="eligibility-bg-div"></div>
                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec8_card1_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_field('sec8_card1_desc'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-4 col-12 text-center" style="width: 18rem;">
                    <div class="card-inner  p-4">
                        <img src="<?php the_field('sec8_card2_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="eligibility-bg-div"></div>


                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec8_card2_heading'); ?></h6>
                          <p class="card-text fc-slate"><?php the_field('sec8_card2_desc'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-4 col-12 text-center" style="width: 18rem;">
                    <div class="card-inner  p-4">
                        <img src="<?php the_field('sec8_card3_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="eligibility-bg-div"></div>

                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec8_card3_heading'); ?></h6>
                          <p class="card-text fc-slate px-4"><?php the_field('sec8_card3_desc'); ?></p>
                        </div>
                    </div>
                </div>
             
                        
            </div>
        </div>
    </section>

    <?php else: ?>

    <section class="w-100 pb-md-3 eligibility " id="product-eligibility">
        <div class="container py-3">
            <div class="row">  
                <div class="col-12 text-center pt-md-5 pt-3">
                    <h5 class="fw-600 pl-1  fc-slate2 text-uppercase"><?php the_field('section8_heading'); ?></h5>
                </div>
                <div class="card col-md-3 col-12 text-center" style="width: 18rem;">
                    <div class="card-inner  p-4">                
                        <img src="<?php the_field('sec8_card1_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="eligibility-bg-div"></div>
                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec8_card1_heading'); ?></h6>
                          <p class="card-text fc-slate fs-13"><?php the_field('sec8_card1_desc'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-3 col-12 text-center" style="width: 18rem;">
                    <div class="card-inner  p-4">
                        <img src="<?php the_field('sec8_card2_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="eligibility-bg-div"></div>


                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec8_card2_heading'); ?></h6>
                          <p class="card-text fc-slate fs-13"><?php the_field('sec8_card2_desc'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-3 col-12 text-center" style="width: 18rem;">
                    <div class="card-inner  p-4">
                        <img src="<?php the_field('sec8_card3_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                        <div class="eligibility-bg-div"></div>

                        <div class="card-body p-0">
                          <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec8_card3_heading'); ?></h6>
                          <p class="card-text fc-slate fs-13"><?php the_field('sec8_card3_desc'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-md-3 col-12 text-center" style="width: 18rem;">
                        <div class="card-inner  p-4">
                            <img src="<?php the_field('sec8_card4_image'); ?>" class="pb-md-4 pb-3 position-relative" alt="...">
                            <div class="eligibility-bg-div"></div>
    
                            <div class="card-body p-0">
                              <h6 class="card-title fc-slate fw-600 lh-24 mb-1"><?php the_field('sec8_card4_heading'); ?></h6>
                              <p class="card-text fc-slate fs-13"><?php the_field('sec8_card4_desc'); ?></p>
                            </div>
                        </div>
                    </div>
                        
            </div>
        </div>
    </section>
<?php endif; ?>
	
		
	<?php if ( is_page('online-checkout-finance') || is_page('merchant-checkout-finance')  ): ?>
	
	<section class="w-100 our-partners py-md-5 py-3">
    <div class="container">
        <div class="row">   
            <div class="col-12">
                <h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase text-center"><?php the_field('our_partners_heading'); ?></h5>
            </div>
            <div class="rio-promos-about col-12 mb-0 mt-md-4">
				
				<?php if( have_rows('our_partners_logo') ): ?>
				<?php while( have_rows('our_partners_logo') ): the_row();?>
				<div class="col">
                <img src="<?php the_sub_field('logo_image'); ?>" class="w-100"/>
					
					
				</div>


				<?php endwhile; ?>
				<?php endif; ?>

            </div>
        </div>
    </div>
</section>
	
	<?php else: ?>
<?php endif; ?>
	
                 
             
<section class="faq-product w-100 " id="product-faq">
        <div class="container">
            <div class="row">
                <div class="col-md-10 p-3 p-md-5 fc-slate-90">
                    <h5 class="fc-slate fw-600 text-uppercase pb-3 pt-3 pt-md-0"><?php the_field('section9_heading'); ?></h5>
					
                    <div class="demo p-3">

	<?php if( have_rows('section9_card') ): ?>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<?php $i=1; while ( have_rows('section9_card') ) : the_row(); ?>
                    
                            <div class="panel panel-default mb-3">
                                <div class="panel-heading" role="tab" id="headingOne-<?php echo $i; ?>">
                                    <h4 class="panel-title mb-0">
                                        <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne-<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="far fa-plus-square"></i>
                                          
                                            <span class="pl-2"><?php the_sub_field('sec9_card_questions'); ?></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne-<?php echo $i; ?>" class="panel-collapse collapse <?php if ($i==1) { echo ''; } ?>" role="tabpanel" aria-labelledby="headingOne-<?php echo $i; ?>">
                                    <div class="panel-body p-4"><?php the_sub_field('sec9_card_answer'); ?></div>
                                </div>
                            </div>
                    
                          
                        
                    <?php $i++; endwhile; ?>
                        </div><!-- panel-group -->
						<?php endif; ?>
                        
                        
                    </div><!-- container -->
                    
                   


                </div>
     
            </div>
        </div>
</section>
	
	
	
	
<?php if ( is_page('loans-against-card-swipes') || is_page('term-finance')  ): ?>

<section class="w-100 sub-products py-md-5 py-3" id="product-sp">
        <div class="container">
            <div class="row">   
                <div class="col-12 ">
                  <h5 class="fw-600 pt-4 pt-md-0 pb-3 text-uppercase text-center fc-slate"><?php the_field('section10_heading'); ?></h5>          
                 </div>
                <div class="rio-promos-sub-products col-12 col-md-10 mx-auto">
					
					<?php if( have_rows('section10_card') ): ?>
					<?php while( have_rows('section10_card') ): the_row();?>

					
                    <div class="card" style="width: 18rem;">  
						<a href="<?php the_sub_field('sub_products_link'); ?>">
                        <div class="position-relative img-div"> 
                            <img src="<?php the_sub_field('sec10_card_image'); ?>" alt="our-products-image" class="w-100 mx-auto">
                        </div>
                        <div class="card-body pt-3 pb-0 pl-0">    
                            <h6 class="card-title fc-slate mb-1 fw-600"><?php the_sub_field('sec10_card_heading'); ?></h6>    
                            <p class="ls-1 fc-slate pt-1 m-0 pr-5"><?php the_sub_field('sec10_card_desc'); ?></p>
                        </div>
							</a>
                </div>
					
          <?php endwhile; ?>					

<?php endif; ?>
				
                                  
                </div>
            </div>
        </div>
    </section>
    <?php else: ?>
<?php endif; ?>

<?php if ( is_page('term-finance') || is_page('loans-against-card-swipes') || is_page('school-finance') || is_page('doctor-loans') ): ?>

   
<section class="w-100 bl-product py-3 pb-md-5 cities" id="product-li">
        <div class="container">
            <div class="row">  
                <div class="col-md-8 mx-auto">
                    <div class="row">
					
						<?php if( have_rows('section11_branches') ): ?>
	<?php while( have_rows('section11_branches') ): the_row();?>
						<div class="col-md-6 pb-md-3 pb-2">
							<a href="<?php the_sub_field('location_link'); ?>" class="fc-slate"><?php the_sub_field('sec11_location'); ?></a>
						</div>
						<?php endwhile; ?>

<?php endif; ?>
                       
                    </div>
                </div> 
            </div>
        </div>
    </section>

<?php else: ?>
<?php endif; ?>
	

</main>
<?php get_footer(); ?>