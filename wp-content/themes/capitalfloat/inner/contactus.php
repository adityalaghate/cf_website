<?php /* Template Name: contactus */ ?>
<?php get_header(); ?>
        
<main>

<section class="w-100 pt-md-5 pt-3 mb-md-5 contactus-main position-relative">
    <div class="container">
        <div class="row">   
				<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-h.png" alt="patern-image" class="contact-pattern position-absolute d-none d-md-block">
            <div class="col-md-6 contact-address mb-3 mb-md-0">
                <h5 class="fw-600 pb-2 fc-slate2 text-uppercase">Capital float Headquarters</h5>
				<p>
					Capital Float is the trade name of CapFloat Financial Services Pvt. Ltd. (Formerly known as Zen Lefin Private Limited)
				</p>
                <p class="m-0 fw-600">Gokaldas Platinum,</p>
                <address class="fs-13 fw-600">                        
                        New no. 3 (Old no. 211), Upper Palace Orchards, Bellary Road, Sadashiva Nagar, Bengaluru - 560080.
                </address>
                <ul class="p-0">
                    <li class="fs-13 pb-3 mail">
                        <span class="fw-600 text-black pr-2">Email:</span><a href="mailto:info@capitalfloat.com" class="fc-black">info@capitalfloat.com</a>
                    </li>
                    <li class="fs-13 telephone">
                            <span class="fw-600 text-black pr-2">Tel:</span>1860 419 0999
                        </li>               
                </ul>



                <div class="pt-md-5 mt-md-4 pt-3 mt-2 locateus">
                    <h6 class="pb-2 fc-slate2 fw-500">Locate Us in More Cities</h6>
                    <div class="input-group pb-3">
                            <select onchange="admSelectCheck(this)" class="custom-select" id="inputGroupSelect04" aria-label="Example select with button addon">
                              <option value="empty" id="">Select City</option>								
                              <option value="18" id="c-ahmedabad">Ahmedabad</option>
                              <option value="0" id="c-bangalore">Bangalore</option>
                              <option value="1" id="c-chandigarh">Chandigarh</option>
                              <option value="2" id="c-chennai">Chennai</option>
                              <option value="3" id="c-cochin">Cochin</option>
                              <option value="4" id="c-coimbatore">Coimbatore</option>
                              <option value="5" id="c-delhi">Delhi</option>
                              <option value="6" id="c-hyderabad">Hyderabad</option>
                              <option value="7" id="c-indore">Indore</option>
                              <option value="8" id="c-jaipur">Jaipur</option>
                              <option value="9" id="c-kolkata">Kolkata</option>
                              <option value="10" id="c-lucknow">Lucknow</option>
                              <option value="11" id="c-madurai">Madurai</option>
                              <option value="12" id="c-mumbai">Mumbai</option>
                              <option value="13" id="c-pune">Pune</option>
                              <option value="14" id="c-salem">Salem</option>
                              <option value="15" id="c-surat">Surat</option>
                              <option value="16" id="c-vadodara">Vadodara</option>
                              <option value="17" id="c-visakhapatnam">Visakhapatnam</option>								
                            </select>
                            <div class="input-group-append">
                              <button class="btn btn-outline-secondary" type="button"><img src="/wp-content/themes/capitalfloat/images/locate-search.png" /></button>
                            </div>
                          </div>
                          <address class="pr-3 fc-slate2 fs-13 mb-2" id="Ahmedabad-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                607, ABC-2, Near St. Xavier's College, 
                                Opp. Wagh Bakri Tea Lounge/ Flamingo Transworld,Off CG Road, 
                                Navrang pura, Ahmedabad-380006.
                          </address>
					 <address class="pr-3 fc-slate2 fs-13 mb-2" id="bangalore-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                New No. 3 (Old 211), Gokaldas Platinum, 
                                Upper Palace Orchards, Bellary Road, Sadashivanagar, 
                                Bangalore – 560080.
                          </address>
					 <address class="pr-3 fc-slate2 fs-13 mb-2" id="Chandigarh-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                Office no.419& 421, Level 4 , Tower A,                                 
                                Godrej Eternia, Plot No. 70, Industrial Area -1,
						 		Chandigarh-160002.
                          </address>
					 <address class="pr-3 fc-slate2 fs-13 mb-2" id="Chennai-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                Shyamala Towers, 6th Floor, East Wing,
                                136, Arcot Road,Saligramam,
                                Chennai-600093.
                          </address>
					<address class="pr-3 fc-slate2 fs-13 mb-2" id="Cochin-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                6th Floor GK Arcade,
                                Palarivttom Pipeline Signal Junction,
                                Cochin-682028.
                          </address>
					<address class="pr-3 fc-slate2 fs-13 mb-2" id="Coimbatore-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                Aurum, Ground Floor, No.9, 
                                Kannusamy Road, R.S.Puram, 
                                Coimbatore - 641002.
                          </address>
					<address class="pr-3 fc-slate2 fs-13 mb-2" id="Delhi-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                B-36, Opposite Metro Pillar- 125,
                                Pusa Road, Block 11, Old Rajinder Nagar,
                                New Delhi-110060.
                          </address>
					<address class="pr-3 fc-slate2 fs-13 mb-2" id="Hyderabad-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                S B Towers, 4th Floor, 6-3-354 , 
                                Road No 1 , Banjara hills,
                                Hyderabad , Telangana-500034.
                          </address>
					<address class="pr-3 fc-slate2 fs-13 mb-2" id="Indore-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                No.309, Regus Business Centre,
                                DNR 90, YN Road,
                                Near Monica Galaxy, Indore- 452001.
                          </address>
					<address class="pr-3 fc-slate2 fs-13 mb-2" id="Jaipur-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                701-A, 7th Floor, Ambition Tower,
                                Agarsen Circle, Subhash Marg,
                                C-Scheme, JAIPUR - 302001.
                          </address>
						<address class="pr-3 fc-slate2 fs-13 mb-2" id="Kolkata-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                Room No,118, 1st Floor, The Legacy, 
                                25 A,Shakespeare Sarani,
                                Kolkata - 700017. India
                          </address>
						<address class="pr-3 fc-slate2 fs-13 mb-2" id="Lucknow-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                410, 4th Floor,
                                Halwasiya Court, Hazaratganj,
                                Lucknow -226001.
                          </address>
						<address class="pr-3 fc-slate2 fs-13 mb-2" id="Madurai-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                No 39,First Floor,
                                Zion Towers,Kalavasal Junction,
                                Bye Pass Road, Madurai-16.
                          </address>
							<address class="pr-3 fc-slate2 fs-13 mb-2" id="Mumbai-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                407,  Satellite Gazebo,B Wing, Fourth Floor, 
                                Next to Solitaire Corporate Park,
                                Chakala, Andheri East, Mumbai 400093.
                          </address>
						<address class="pr-3 fc-slate2 fs-13 mb-2" id="Pune-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                Office No F4, 1st Floor,
                                Nucleus Mall, Jee Jee Bhoy Towers,
                                Sadhu Wasvani Road,Agarkar Nagar,
								Pune-411001,Maharashtra. 
                          </address>
						<address class="pr-3 fc-slate2 fs-13 mb-2" id="Salem-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                Jayam Arun Plaza ( Century Plaza),
                                No.30/1, 1 St Floor, Thulasi Medical Complex,Advaitha Ashram Road,
                                Fairlands, Salem- 636016.
                          </address>
						<address class="pr-3 fc-slate2 fs-13 mb-2" id="Surat-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                412/414, Western Business Park,
                                Vesu Circle, Opposite SP Jain School,
                                Vesu, Surat- 395007.
                          </address>
						<address class="pr-3 fc-slate2 fs-13 mb-2" id="Vadodara-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                B 307, Altlantis, 
                                Beside Central Square,
                                Sarabhai Main Road, Vadodra- 390007.
                          </address>
					<address class="pr-3 fc-slate2 fs-13 mb-2" id="Visakhapatnam-address" style="display:none;">
                                <p class="fw-600 m-0">Capital Float, CapFloat Financial Services Pvt. Ltd.</p>
                                Chamber 6, Qubexpro Business Center,
                                V Mall, Above Lifestyle, 4th Floor,
                                Facor Layout, Waltair Uplands,
								Visakhapatnam – 530003
                          </address>
					
					
                          <a class="fw-600 fc-blue fs-13" href="#">View Directions</a>
                </div>
            </div>
<!--             <div class="col-md-6 col-12">
				<h4 class="fc-slate fw-600 pb-4 text-uppercase pl-md-3 ml-md-1">
					write to us
				</h4>
                				<?php echo do_shortcode('[contact-form-7 id="1709" title="contactus-innerpages"]'); ?>				<p class="fs-14 fc-slate m-0 pt-3 pb-md-5 pl-md-3 ml-md-1 pb-3">
					We are happy to answer all your questions. Contact us for any queries or feedback you have regarding our DSA partner program
				</p>

            </div> -->
			   <div class="col-md-6 col-12">
				   <img src="/wp-content/uploads/2019/10/IndiaMap-1.png" class="w-100 mb-3"/>
			

            </div>
			
           
        </div>
    </div>
</section>
	<section class="w-100 inner-contactus py-md-5 py-3" style="background-image: url(/wp-content/themes/capitalfloat/images/contactus-bg.png)">
	<div class="container">
		<div class="row">
			<div class="col-12 text-container">
				<h5 class="fc-slate fw-600 text-uppercase pb-2">contact us now</h5>
                <p class="fs-14 pb-md-3 pb-2">We are happy to answer your questions. Please submit your query below and we will get in touch with you.</p>				
			</div>
			<div class="col-12">
				<?php echo do_shortcode('[contact-form-7 id="1709" title="contactus-innerpages"]'); ?>				
			</div>
			<div class="col-md-7 d-flex justify-content-center contact-faq">
				<p class="fs-14 fc-slate m-0">
					For any queries, go to <a href="#" class="fs-14 fc-slate fw-800">FAQ’s</a>
				</p>
							
			</div>
			
		</div>		
	</div>
</section>

             



      </main>
<?php get_footer(); ?>