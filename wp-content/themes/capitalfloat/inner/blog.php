   <?php /* Template Name: blog */ ?>
<?php get_header(); ?>

 <section class="w-100 mt-5 pt-md-5">
            <div class="container text-center">
           
                <div class="row pt-5">
<?php
$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;

$args = array( 'paged'=> $paged, 'posts_per_page' => 9);
query_posts($args);
//foreach( $recent_posts as $recent ){?>
<?php if ( have_posts() ) : while (have_posts()) : the_post(); 
					?>
	<div class="col-md-4 d-flex justify-content-stretch mb-md-4 mb-3">
			<div class="card ">
				<img class="card-img-top" src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'full');?>" alt="Card image cap"/>
				<div class="card-body">
					<figcaption class="small text-left fs-13 figure-caption pb-2"><?php echo get_the_date(); ?></figcaption>

					<h5 class="card-title text-left fs-16">
						<a href="<?php echo get_permalink(get_the_ID()); ?>"><?php echo get_the_title(); ?></a></h5>
					
						<div class="card-text text-left fs-13 line-clamp"><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php echo get_the_content();?></a>
					</div>


				</div>
			</div>
	 </div>
<?php //}?>      
<?php endwhile; ?>
<!-- pagination -->
<?php
global $wp_query;
echo paginate_links( array(
	'base' => '/blog/%_%',
	'format' => '%#%/',
	'current' => max( 0, get_query_var('paged')),
	'total' => $wp_query->max_num_pages,
	'type'=> 'list'
) );					
?>
<?php else : ?>
<!-- No posts found -->
<?php endif; ?>
	 </div>
            </div>
       
        </section>

<?php get_footer(); ?>