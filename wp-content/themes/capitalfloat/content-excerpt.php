<?php
/**
 * Template part for displaying posts with excerpts
 *
 * Used in Search Results and for Recent Posts in Front Page panels.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage finaxar
 * @since 1.0
 * @version 1.2
 */
if($post->post_type == "sme_articles"){?>
	<div class="col-md-6">
			<div class="card">
				<img class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" />
				<div class="card-body">
					<h5 class="card-title light-blue font-17">
						<a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
						</a>  
					</h5>
					<div class="card-text font-sourcesanspro truncate"><?php the_content(); ?></div>

					<p><span>by <?php the_author(); ?></span> | <span><?php echo the_date();?> </span></p>
					<div class="post-category">
						<?php the_category(' '); ?>
					</div>
				</div>
			</div>
	</div>
<?php }
else if($post->post_type == 'sme_guides'){?>
	<div class="row box-shadow-1">
      
		<div class='col-md-6 p-0'>
        <img class="card-img-top guide-thumbnail" src="<?php the_post_thumbnail_url();?>" alt="Card image cap"/>
		</div>
		  <div class='col-md-6'>
			  
			<div class="card-body">
			  <h5 class="card-title text-left ">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
			  <a href="<?php the_permalink(); ?>" class='card-text font-sourcesanspro black2 font-17 truncate'><p class="card-text text-left fs-13 line-clamp"><?php the_content();?>               </p></a>

				<div class="post-category">
				<?php the_category(' '); ?>
				</div>
				<div class='download mt-3'>
							<a href='<?php the_field("download_guide");?>' class='btn btn-secondary rounded-pill btn-gradient3'>Download Guide</a>
						</div>
			</div>
			  
		  </div>
      
   </div>
<?php }
else{ // For FAQ
	$categories = get_the_category($post->ID);
	$categoriesArr = array();
	foreach($categories as $cat){
		if(!in_array($cat, $categoriesArr)){
			$categoriesArr[] = $cat->cat_ID;	
		}
	}
	
	foreach($categoriesArr as $category){
?>
<div id="accordion" class="row FAQ-accordion">
        <div class="card col-12">
          <div class="card-header bg-transparent p-0" id="headingOne">
            <div class="bottom-accordion-icon"></div>
            <div class="mx-4 py-3 text-left border-bottom-0 collapsed" data-toggle="collapse" data-target="#FAQ-<?php echo $category;?>" aria-expanded="false" aria-controls="collapseOne">
              <p><?php echo get_the_category_by_ID($category);?></p>
            </div>
          </div>
          <div id="FAQ-<?php echo $category;?>" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
        
              <ol>
				<li>
				  <?php the_title();?>
				  <span class="d-block mb-2 mt-1"><?php the_content();?></span>
				  <div class="mb-3 d-flex align-items-center">
					<?php echo get_avatar(get_the_author_meta('ID'), 45);?>
					<div>
					<span class="d-block">Written by <?php the_author();?></span>
					<span class="d-block">Uptated over <?php echo human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ).' '.__( 'ago' );?> </span>
					</div>
				  </div>
				</li>
              </ol>

            </div>
          </div>
        </div>
      </div>
<?php }
}?>