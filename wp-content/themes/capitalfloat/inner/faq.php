<?php /* Template Name: faq */ ?>
<?php get_header(); ?>
        
<main>
	<section class="w-100 pt-md-5 pt-3 mb-3 mb-md-5 faq">
    <div class="container">
        <div class="row">   
            <div class="col-12 pb-4">
                    <ul class="nav nav-pills mb-3 d-flex align-items-center" id="pills-tab" role="tablist">
                <span class="pr-3 fc-slate fw-600">Filter: </span>

                            <li class="nav-item">
                              <a class="nav-link tab-btn active text-uppercase" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Business Loans</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link tab-btn text-uppercase" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Consumer Loans</a>
                            </li>
                          
                          </ul>
                         
          
            </div>
            <div class="col-12 pr-0">
                    <div class="tab-content faq-content" id="pills-tabContent">

                        <!------------------------------------------------------1st Tab Starts here ----------------------------------------->

                            <div class="tab-pane fade show active row col-12" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

                                    <div class="nav flex-column nav-pills col-3 text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <a class="nav-link active mb-4" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Our Loans</a>
                                            <a class="nav-link mb-4" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Eligibility</a>
                                            <a class="nav-link mb-4" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">The Process</a>
                                            <a class="nav-link mb-4" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">About us</a>
                                          </div>


                                          <div class="tab-content col-9" id="v-pills-tabContent">
                                       
                                                <div class="input-group mb-3 w-50">
                                                        <input type="text" id="myInput" class="form-control" placeholder="Search questions" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                                        <div class="input-group-append">
                                                          <span class="input-group-text" id="basic-addon2"><img src="/wp-content/themes/capitalfloat/images/search-gray.png" /></span>
                                                        </div>
                                                      </div>
                                                      


                                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                                
                                                               
             

                                                <div class="fc-slate-90 w-100" id="myTable">
                                                    <div class="demo">

                                    
                                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                    
                                                            <div class="panel panel-default mb-3">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title mb-0">
                                                                        <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                            <i class="far fa-plus-square"></i>
                                                                            <!-- <i class="far fa-minus-square"></i> -->
                                                                            <span class="pl-3">Is a Capital Float loan right for my business?</span>
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body p-4">
                                                                            We help businesses by providing short-term, flexible loans in a simple and efficient manner. If you run a small or medium enterprise with frequent working capital needs, a Capital Float loan will quickly provide you the necessary funds to manage cash flow, pay suppliers, or invest in growth.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    
                                                            <div class="panel panel-default mb-3">
                                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                                    <h4 class="panel-title mb-0">
                                                                        <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                            <i class="far fa-plus-square"></i>
                                                                            <!-- <i class="far fa-minus-square"></i> -->
                                                                            <span class="pl-3">How much can I borrow and for how long?</span>
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                    <div class="panel-body p-4">
                                                                            Capital Float offers loans ranging from ₹3 lakhs – ₹1 crores, with terms between 6 months – 36 months.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    
                                                            <div class="panel panel-default mb-3">
                                                                <div class="panel-heading" role="tab" id="headingThree">
                                                                    <h4 class="panel-title mb-0">
                                                                        <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                            <i class="far fa-plus-square"></i>
                                                                            <!-- <i class="far fa-minus-square"></i> -->
                                                                        <span class="pl-3"> How much does it cost?</span>
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                    <div class="panel-body p-4">
                                                                            Typically, our rates start at 15% per annum with a processing fee of up to 2% of the loan amount. Start an application here to see what rate you can borrow at.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    
															 <div class="panel panel-default mb-3">
                                                                <div class="panel-heading" role="tab" id="headingFour">
                                                                    <h4 class="panel-title mb-0">
                                                                        <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                                            <i class="far fa-plus-square"></i>
                                                                            <!-- <i class="far fa-minus-square"></i> -->
                                                                        <span class="pl-3">Do I need to pledge property or machinery to get a loan?</span>
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                                    <div class="panel-body p-4">
                                                                            No. We evaluate businesses solely on the strength of their cash flows and expected receivables.
                                                                    </div>
                                                                </div>
                                                            </div>
															
															<div class="panel panel-default mb-3">
                                                                <div class="panel-heading" role="tab" id="headingFive">
                                                                    <h4 class="panel-title mb-0">
                                                                        <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                                            <i class="far fa-plus-square"></i>
                                                                            <!-- <i class="far fa-minus-square"></i> -->
                                                                        <span class="pl-3">How do I repay a Capital Float loan?</span>
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                                    <div class="panel-body p-4">
                                                                            We offer loan products with flexible repayment plans, so you can choose what makes sense for your business. Loans against receivables can be paid back in a single "bullet" installment at maturity, while unsecured loans are repayable via EMIs. All repayments can be made either via ECS or a post-dated cheque.
                                                                    </div>
                                                                </div>
                                                            </div>
															
															<div class="panel panel-default mb-3">
                                                                <div class="panel-heading" role="tab" id="headingSix">
                                                                    <h4 class="panel-title mb-0">
                                                                        <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                                                            <i class="far fa-plus-square"></i>
                                                                            <!-- <i class="far fa-minus-square"></i> -->
                                                                        <span class="pl-3">Can I repay my loan before the due date?</span>
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                                    <div class="panel-body p-4">
                                                                            Yes. however, we do charge a minor fee for preclosure of loans.
                                                                    </div>
                                                                </div>
                                                            </div>
															
                                                        </div><!-- panel-group -->
                                                        
                                                        
                                                    </div><!-- container -->
                                                    
                                                


                                                </div>
                                            
         
                                            </div>
                                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">

                                                    <div class="fc-slate-90 w-100" id="myTable">
                                                            <div class="demo">
        
                                            
                                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">What kinds of businesses does Capital Float lend to?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                            <div class="panel-body p-4">
                                                                                    We work with small & medium enterprises across industries, including e-commerce, textiles & apparel, healthcare, auto and engineering, food processing, software, facilities management, and professional services. Our borrowers are typically fast-growing SMEs with one or more well-reputed clients (e.g., MNCs, large E-Commerce players, publicly listed Indian corporates).
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingTwo">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">How do I find out if I’m eligible?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                            <div class="panel-body p-4">
                                                                                    The eligibility for our loans varies from product to product. Visit our product pages of your choice to read a detailed description of the eligibility criteria for the product.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingThree">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">My business is less than 3 years old. Can I still get a loan from Capital Float?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                            <div class="panel-body p-4">
                                                                                    Yes. We actively support early-stage businesses led by capable and driven entrepreneurs. Unlike most banks and NBFCs today, we are happy to work with companies that have been operational for less than 3-5 years, so long as they have strong management and a good financial track record.
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	  <div class="panel panel-default">
                                                                        <div class="panel-heading" role="tab" id="headingFour">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">I’ve been denied by my bank before. Can I still get a loan from Capital Float?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                                            <div class="panel-body p-4">
                                                                                    Yes. Our approach to lending evaluates SMEs in a very different way from banks. Rather than focusing on traditional factors such as collateral such as property, we look at the strength of your cash flows and customer relationships. This means that we can lend to many business that have been denied by their banks.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                </div><!-- panel-group -->
                                                                
                                                                
                                                            </div><!-- container -->
                                                            
                                                        
        
        
                                                        </div>



                                            </div>
                                            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                                
                                                    <div class="fc-slate-90 w-100" id="myTable">
                                                            <div class="demo">
        
                                            
                                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">What is the application process?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                            <div class="panel-body p-4">
                                                                                    You can apply for a Capital Float loan from your desk within 10 minutes via our online application. Once you submit the application, a representative will visit your office to collect the necessary KYC documents or you can upload the files during the time of application. We typically process all applications within 3 days from submission and immediately transfer the funds to your account. Questions? Give us a call on 1860 419 0999.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingTwo">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">What information and documents are required to apply?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                            <div class="panel-body p-4">
                                                                                    Documents required during application vary from product to product. Please visit the relevant product page to read more about the documents required pertaining to that particular product. 
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingThree">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">How do I know this is a secure website?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                            <div class="panel-body p-4">
                                                                                    We take the security of your information very seriously and have taken care to comply with the best practices in the industry. You can be assured that all the data provided is in good hands and the information is handled in a secure manner.
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	 <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFour">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">How quickly will I get a decision and receive funds?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                                            <div class="panel-body p-4">
                                                                                    We typically provide funding in 3 days. The funds will be transferred to your designated bank account on the same day as approval.

                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																		 <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFive">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">How do I check the status of my loan application?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                                            <div class="panel-body p-4">
                                                                                    Login into www.capitalfloat.com to see a customized dashboard with your loan application status for all current and future loans.

                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingSix">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">How do I check my account balance and due date?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                                            <div class="panel-body p-4">
                                                                                    Login into www.capitalfloat.com to see a customized dashboard with details for all your current loans.

                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default">
                                                                        <div class="panel-heading" role="tab" id="headingSeven">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">How easy is it to apply for another loan if I need funds again?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                                            <div class="panel-body p-4">
                                                                                    Once you are approved to join the Capital Float platform and successfully complete one loan cycle, future financing is literally at your fingertips. As and when you require funds, apply online within 10 minutes for new loans and receive funds in your bank account within 48 hours.

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                </div><!-- panel-group -->
                                                                
                                                                
                                                            </div><!-- container -->
                                                            
                                                        
        
        
                                                        </div>


                                            </div>
                                            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                                
                                                    <div class="fc-slate-90 w-100" id="myTable">
                                                            <div class="demo">
        
                                            
                                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">What is Capital Float?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                            <div class="panel-body p-4">
                                                                                    <p>
																						
																				Capital Float is the pioneer of digital lending in India. The company leverages technology and big data analytics to deliver innovative credit products to businesses and individuals across the country. Through its proprietary digital loan underwriting and origination expertise, it has been able to deliver financing solutions to vast sections of the economy traditionally underserved by large financial institutions. It has developed partnerships to finance SMEs across various ecosystems such as e-commerce, manufacturing supply chains, retail, travel and hospitality, and digital remittances. Capital Float has also partnered with leading brands to offer finance to consumers availing products and services in segments such as education, vocational training, wellness, elective healthcare, home furnishings, electrical equipment, lifestyle, travel, fitness and consumer durables.</p>
																				<p>
																					
																			
																				Founded in 2013, Capital Float is the trade name for Capfloat Financial Services Private Limited (formerly known as Zen Lefin Private Limited), a Non-Banking Finance Company (NBFC) registered with the Reserve Bank of India. The company has raised funding from marquee investors such as SAIF Partners, Sequoia India, Aspada Investments, Creation Investments Capital Management LLC, Ribbit Capital, and Amazon. Capital Float is headquartered in Bangalore, with offices in Mumbai, Delhi NCR and other cities.
For more information: <a href="www.capitalfloat.com">www.capitalfloat.com	</a></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingTwo">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">Where is Capital Float based?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                            <div class="panel-body p-4">
                                                                                    Capital Float is headquartered in Bangalore, India. Our office address is New no. 3 (Old no. 211), Upper Palace Orchards, Bellary Road, Sadashiva Nagar, Bengaluru - 560080.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingThree">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3"> Why is a Capital Float loan better than from a bank or another NBFC?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                            <div class="panel-body p-4">
                                                                                    Here are some of the reasons we believe a loan from Capital Float is more conducive for SMEs.
				<ul class="pt-3">
					<li>No requirement of property or machinery as collateral</li>
					<li>Willing to lend to businesses less than 3-5 years old</li>
					<li>Easy, quick, paperless online application process</li>
					<li>Funds in your account within 3 days</li>
					<li>Flexible loan tenures starting from 6 months - 36 months</li>
					<li>Low pre-closure penalties</li>
					<li>Flexible repayment options</li>
					<li>Interest rates at par with effective rates charged by banks</li>
					<li>No hidden fees (e.g., life insurance)</li>
					

				</ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	  <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFour">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Why is a Capital Float loan better than a moneylender?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                                            <div class="panel-body p-4">
                                                                                    Here are some of the reasons we believe a loan from Capital Float better when compared to availing finance from a moneylender. 
				<ul class="pt-3">
					<li>Significantly lower interest rates</li>
					<li>Get funds equally fast without requiring personal introductions</li>
					<li>Free of risks typically associated with informal sector finance</li>
					<li>Less burdensome repayment schedules</li>
				</ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	
																		  <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFive">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Why is a Capital Float loan better than a credit card?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                                            <div class="panel-body p-4">
                                                                                   Here are some of the reasons we believe a loan from Capital Float is more conducive for SMEs when compared to using a credit card. 
				<ul class="pt-3">
					<li>Significantly lower interest rates (credit card debt at approx. 40% p.a.)</li>
					<li>Dependable source of credit for regular business needs</li>
					<li>Loan sizes set according to business needs, not credit card limits</li>
					
				</ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	
																	
																	
																	
																	
                                                            
                                                                </div><!-- panel-group -->
                                                                
                                                                
                                                            </div><!-- container -->
                                                            
                                                        
        
        
                                                        </div>

                                                        
                                            </div>
                                          </div>

                            </div>

                        <!------------------------------------------------------1st Tab Ends here ----------------------------------------->

                        <!------------------------------------------------------2nd Tab Starts here ----------------------------------------->
                        

                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                
                                    <div class="nav flex-column nav-pills col-3 text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <a class="nav-link active mb-4" id="v-pills-cl-about-tab" data-toggle="pill" href="#v-pills-cl-about" role="tab" aria-controls="v-pills-cl-about" aria-selected="true">About</a>
                                            <a class="nav-link mb-4" id="v-pills-cl-reg-tab" data-toggle="pill" href="#v-pills-cl-reg" role="tab" aria-controls="v-pills-cl-reg" aria-selected="false">Registration</a>
                                            <a class="nav-link mb-4" id="v-pills-cl-transaction-tab" data-toggle="pill" href="#v-pills-cl-transaction" role="tab" aria-controls="v-pills-cl-transaction" aria-selected="false">Transactions</a>
                                            <a class="nav-link mb-4" id="v-pills-cl-cs-tab" data-toggle="pill" href="#v-pills-cl-cs" role="tab" aria-controls="v-pills-cl-cs" aria-selected="false">Customer Service</a>
                                          </div>
                                          <div class="tab-content col-9" id="v-pills-tabContent">

                                                <div class="input-group mb-3 w-50">
                                                        <input type="text" id="myInput1" class="form-control" placeholder="Search questions" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                                        <div class="input-group-append">
                                                          <span class="input-group-text" id="basic-addon2"><img src="/wp-content/themes/capitalfloat/images/search-gray.png" /></span>
                                                        </div>
                                                      </div>


                                            <div class="tab-pane fade show active" id="v-pills-cl-about" role="tabpanel" aria-labelledby="v-pills-cl-about-tab">

                                                    <div class="fc-slate-90 w-100" id="myTable1">
                                                            <div class="demo">
        
                                            
                                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">What is Online Checkout Finance?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                            <div class="panel-body p-4">
                                                                                    Online Checkout Finance is the hassle-free way to get instant credit, via a completely digital process, for purchases made online. You have to complete one-time setup process, which should not take more than 2 minutes and doesn't require credit card. Once the setup is complete, you can avail Online Checkout Finance limit at the payments page on the partner website to make payments over the EMIs ranging from 3 to 12 months. Here are the key benefits of Online Checkout Finance?
		<ul class="pt-3">
			<li>Avail credit within 2 minutes. Get instant decision</li>
			<li>Completely digital process</li>
			<li>Credit card not required</li>
			<li>No processing or cancellation fee</li>
			<li>No pre-closure charges</li>
			<li>Seamless checkout on our partner website using this payment mode</li>
		</ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingTwo">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">What are the eligibility criteria to avail Online Checkout Finance?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                            <div class="panel-body p-4">
                                                                                    You need to have an account with our partner website, a valid PAN card, Aadhaar card and Bank account in the listed banks. Your age must be 18 years or above. Further eligibility is decided basis a host of variables like your previous bookings & cancellations history on the partner website, your credit bureau history and so on. Our models will rate your performance basis these variables and accordingly enable or disable this payment mode for you and decide the Online Checkout Finance limit for your account.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingThree">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Where can I check my Online Checkout Finance eligibility and the limit assigned to me?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                            <div class="panel-body p-4">
                                                                                    If you are eligible, you can follow on-screen instructions on our partner website to register for the payment mode and your assigned limit will be displayed to you during this process. 
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	   <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFour">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">If I don't have the credit bureau history (CIBIL score), will I not be applicable for getting Online Checkout Finance limit?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                                            <div class="panel-body p-4">
                                                                                   Your credit bureau history is one of the variables we use in our decisioning process. However, it is not a mandatory variable, and you may still get Online Checkout Finance limit without having bureau history. Subject to other criteria met. 
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	  <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFive">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Why do I need to have bank account in listed banks?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                                            <div class="panel-body p-4">
                                                                                  Bank account is required to set-up monthly auto repayments of EMIs created for your bookings using Online Checkout Finance limit. Once auto repayment is set, you don't have to worry about payment of EMIs on due date. You just have to keep the bank account active and funded for EMI repayments. 
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
                                                            
                                                                </div><!-- panel-group -->
                                                                
                                                                
                                                            </div><!-- container -->
                                                            
                                                        
        
        
                                                        </div>


                                            </div>
                                            <div class="tab-pane fade" id="v-pills-cl-reg" role="tabpanel" aria-labelledby="v-pills-cl-reg-tab">

                                                    <div class="fc-slate-90 w-100" id="myTable1">
                                                            <div class="demo">
        
                                            
                                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">How can I register for Online Checkout Finance?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                            <div class="panel-body p-4">
                                                                                    Go to Online Checkout Finance registration page on our our partner's mobile App and follow these simple on-screen instructions.
																				<h5>
																					Step 1: Verify
																				</h5>
																				<ul>
																					<li>Provide your PAN and Aadhaar/Virtual ID (VID) details for verification. Complete verification by confirming one-time password (OTP) sent on your Aadhaar Linked mobile number.</li>
																					<li>Once the OTP is entered, your profile will be verified and evaluated, and Online Checkout Finance limit will be determined which will be displayed to you on the next screen.</li>
																					</ul>
																					<h5>
																						Step 2: Accept
																					</h5>
																					<p>
																						Your approved Online Checkout Finance limit will be displayed here along with the loan agreement. Please read and accept the agreement to complete Online Checkout Finance registration.

																					</p>
																					<h5>
																						Step 3: Repayment
																					</h5>
																					<p>
																						You will have to link your bank account/debit card to process your monthly EMI repayments whenever you use this limit for booking.
																					</p>
																				<ul class="pt-3">
																					<li>In order to successfully set this up, your bank account/debit card will be charged upto Rs 5 which will be refunded to your bank account within 3-5 business days.</li>
																					<li>
																					Only few banks are supported for now and you can find the list on-screen during set-up</li>
																					
																				</ul>
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingTwo">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">Is PAN Card and Aadhaar/VID mandatory?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                            <div class="panel-body p-4">
                                                                                    PAN and Aadhaar or VID are mandated by regulations for completing basic Know Your Customer (KYC) checks. If you don't have a PAN or Aadhaar/VID, you will not be able to complete the registration. 
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingThree">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">What is Aadhaar Virtual ID (VID)? Can it be used in place of my Aadhaar number?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                            <div class="panel-body p-4">
                                                                                <p>
																					
																				   Aadhaar Virtual ID (VID) is an alternative to your Aadhaar number. You can provide your VID instead of Aadhaar number for KYC completion. This is the new feature implemented by UIDAI (organization responsible for issuing Aadhaar number to citizens of India) considering the privacy of person's Aadhaar number and associated details.</p> 
																				<p>
																					VID is a temporary 16-digit numerical code, which can be generated on UIDAI website against any specific Aadhaar number. It can be generated any number of times and original Aadhaar number cannot be retrieved using this code.
																				</p>
																				<p>
																					
																					More details on VID can be found <a href="https://uidai.gov.in/contact-support/have-any-question.html">here. </a> 
																				</p>

                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	  <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFour">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">How can I generate Aadhaar VID?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                                            <div class="panel-body p-4">
                                                                               
			<p>Step 1:
				Visit UIDAI's website at<a href="http://uidai.gov.in/"></a> http://uidai.gov.in/
																				</p>
																				<p>
																					Step 2: Click on the "Virtual ID (VID) Generator" from Aadhaar Services section
																				</p>
																				<p>
																					Step 3: You will be taken to a new VID Generation page
																				</p>
																				<p>
																					Step 4: Enter your 12 digit Aadhaar Number and the security code
																				</p>
<p>
	Step 5: Now click on the "Send OTP" button
																				</p>
																				<p>
																					Step 6: An OTP will be sent to your mobile number registered with Aadhaar
																				</p>
																				<p>
																					Step 7: Enter the OTP and select the option to either "Generate VID" or "Retrieve VID"
																				</p>
																				<p>
																					Step 8: Now click on the Submit button
																				</p>
																				<p>
																					Step 9: You will get the message on your registered mobile number mentioning the 16-digit virtual ID for Aadhaar number
																				</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	  <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFive">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">I don't remember my Aadhaar number, how do I find it?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                                            <div class="panel-body p-4">
                                                                               
			You don't have to worry in case you forgot your Aadhaar Number. Now you can find your Aadhaar number online within few minutes by following these steps:
																				<ul class="pt-3">
																					<li>Go to the Resident Portal of UIDAI <a href="https://resident.uidai.gov.in/find-uid-eid">here</a></li>
																					<li>Now enter your Full Name, Email or Mobile No in the box</li>
																					<li>Enter the security code and click on Get OTP button</li>
																					<li>You will get an OTP on your registered number, please enter the OTP in the box and click on Verify OTP button</li>
																					<li>You will receive your Aadhaar Number on your mobile number</li>
																					
																					
																				</ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	
																	
																	  <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingSix">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3"> I didn't receive a One-time password (OTP) after entering my PAN and Aadhaar details.</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                                            <div class="panel-body p-4">
               The OTP is sent by Unique Identification Authority of India (UIDAI) to the phone number registered with your Aadhaar card, as per their database. The following might be one of the reasons for not getting the OTP: 
																				<ul class="pt-3">
																					<li>Are you looking at the mobile number registered with your Aadhar card, for the OTP?</li>
																					<li>Do you have proper mobile network connectivity? If not please try later from an area with good mobile network connectivity.Now enter your Full Name, Email or Mobile No in the box</li>
																					<li>If none of the above is true, the delay might be due to server downtime issues. Please try again later.</li>
																					
																					
																					
																				</ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	
																	  <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingSeven">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">How do I update my mobile number linked to Aadhaar?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                                            <div class="panel-body p-4">
             Aadhaar mobile number update requires biometric authentication and it cannot be done by post or online, according to the UIDAI. You need to visit the nearest Permanent Enrolment Center / Aadhaar Kendra.
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	  <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingEight">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">How long is my KYC valid using Aadhaar number or Aadhaar VID?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                                                            <div class="panel-body p-4">
            Current KYC process using OTP verification of your Aadhaar or VID number is valid for 1 year. We are working on enabling other modes of KYC for you to extend the validity of the account for more than a year. 
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	 <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingNine">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">My internet got disconnected while registering. What should I do now?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                                                            <div class="panel-body p-4">
            In such cases, you can restart the Online Checkout Finance registration process. The process will take you to the step you had left unfinished.
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingTen">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">What is Capital Float and why am I providing my details to them?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                                                                            <div class="panel-body p-4">
            Capital Float is a Reserve Bank of India recognized Non-Banking Financial Corporation (NBFC) with whom partner website has tied up to provide Online Checkout Finance service to its customers. This entity is listed in RBI's list of scheduled NBFCs and holds the license to lend money to consumers, retail customers, small and medium businesses. RBI holds regular audit for this company to ensure regulations are being met as per the regulatory guidelines. Capital Float is extending credit limit to you and hence you have to share details with Capital Float. 
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingEleven">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Is the information I share to avail Online Checkout Finance secure?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
                                                                            <div class="panel-body p-4">
           Partner website follows certified data security standards to ensure safety of your data. Capital Float is also following same standards for keeping your data secure. In addition, Capital Float is mandated to follow data security standards as mandated by Indian government regulations.
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	
																		<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingTwelve">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Why did my Online Checkout Finance registration decline?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
                                                                            <div class="panel-body p-4">
         Following are the probable reasons for an Online Checkout Finance registration to get rejected:
																				<ul class="pt-3">
																					<li>Multiple incorrect OTP attempts for Aadhaar verification.</li>
																					<li>No entry of OTP for Aadhaar verification for a specified time-period.</li>
																					<li>You did not accept the final loan agreement post generation of Online Checkout Finance limit.</li>
																					<li>You are currently not eligible as per internal evaluation policies.</li>
																					
																				</ul>
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingThirteen">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Do I have to provide PAN and Aadhaar details every time I make an Online Checkout Finance transaction?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
                                                                            <div class="panel-body p-4">
        Registration is a one-time process. You do not have to share these details every time you make a transaction. 
																				
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
                                                            
                                                                </div><!-- panel-group -->
                                                                
                                                                
                                                            </div><!-- container -->
                                                            
                                                        
        
        
                                                        </div>


                                            </div>
											 
                                            <div class="tab-pane fade" id="v-pills-cl-transaction" role="tabpanel" aria-labelledby="v-pills-cl-transaction-tab">

                                                    <div class="fc-slate-90 w-100" id="myTable1">
                                                            <div class="demo">
        
                                            
                                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">Where can I book using my Online Checkout Finance limit?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                            <div class="panel-body p-4">
                                                                                   Currently you can utilize Online Checkout Finance limit to make bookings on partner website only. We are working to make this limit available for bookings on Redbus and Goibibo. 
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingTwo">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">What can I book using my Online Checkout Finance limit?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                            <div class="panel-body p-4">
                                                                                    Online Checkout Finance is available on flights and hotels bookings. You can check the availability of this payment mode in the EMI options section on the checkout page. 
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingThree">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">How to complete the bookings using Online Checkout Finance limit?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                            <div class="panel-body p-4">
                                                                                    <p>
																						If Online Checkout Finance is available, you have to select "EMI" menu on payment selection page during checkout and choose "Online Checkout Finance" option within "EMI" menu. Post that, choose the EMI plan from list of plans available for your bookings on payment selections page and complete the booking.

																				</p>
																				<p>
																					Online Checkout Finance, as a payment method, can be applied to your booking when the following conditions are met: 
																				</p>
																				<ul>
																					<li>There is sufficient limit available in your Online Checkout Finance account to cover for the booking</li>
																					<li>Your Online Checkout Finance account is Active</li>
																				</ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFour">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Does Online Checkout Finance bookings entail interest cost?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                                            <div class="panel-body p-4">
                                                                                    <p>
																						Currently the Interest Cost is not borne by you, but this may change in future, we will notify you in that case

																				</p>
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFour">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Where will I get to know the interest charges for my booking?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                                            <div class="panel-body p-4">
                                                                                    <p>
																						The interest rate charged to you per the plan will be displayed at the time of placing an order, while choosing EMI plans. After placing the order, you can always visit the Online Checkout Finance dashboard to verify the same.


																				</p>
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFive">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Where will I get to know the interest charges for my booking?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                                            <div class="panel-body p-4">
                                                                                    <p>
																						The interest rate charged to you per the plan will be displayed at the time of placing an order, while choosing EMI plans. After placing the order, you can always visit the Online Checkout Finance dashboard to verify the same.


																				</p>
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingSix">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">What are the available EMI plans for using Online Checkout Finance?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                                            <div class="panel-body p-4">
                                                                                    <p>Online Checkout Finance currently offers 3, 6, 9 and 12 month EMI plans. The applicable plans for your purchase will be displayed on the payment selection screen during checkout. Applicable EMI plans are the ones which are within the time period of your KYC validity.
																				</p>
																				<p>
																					Please note that the KYC done on your Online Checkout Finance using Aadhaar number and OTP stands valid for a year. So, if your KYC is set to expire in 8 months, you will only see EMI plans of 3 and 6 months at checkout.
																				</p>
																				<p>
																					We are working to enable another modes of KYC as well which will have extended validity period (upwards of 5 years). Once those modes are enabled for Online Checkout Finance and if you go through that, then your KYC validity will be increased. 
																				</p>
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingSeven">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Is auto-repayment setup mandatory for booking?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                                            <div class="panel-body p-4">
                                                                                    <p>Yes. This is one-time setup activity which can be done during registration. We will run specific promotions for selected set of customers, from time to time, to waive off this mandatory auto-repayment setup condition for purchasing using Online Checkout Finance limit. However, we strongly recommend you to setup this once for hassle-free repayments experience. 
																				</p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingEight">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">How many EMIs can I have with Online Checkout Finance?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                                                            <div class="panel-body p-4">
                                                                                    <p>You can make multiple purchases using Online Checkout Finance limit, till the time your limit is available for use. Please note that as per regulations, for KYC using Aadhaar number/VID and OTP, you are not allowed to book (including cancelled orders as well) the sum total of Rs 60,000 using Online Checkout Finance limit. We will keep track of your usage on this regulation imposed limit. You will be notified whenever your available regulatory limit becomes lesser than the available Online Checkout Finance limit. 
																				</p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingNine">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Do you charge any fee for purchase?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                                                            <div class="panel-body p-4">
                                                                                    <p>No transaction fee is charged 
																				</p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																		
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingTen">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Do I need to make any down-payment to avail Online Checkout Finance?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                                                                            <div class="panel-body p-4">
                                                                                    <p>No. Down-payments are not required for booking </p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																		<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingEleven">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">How much amount will be deducted from my Online Checkout Finance limit on making a booking?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
                                                                            <div class="panel-body p-4">
                                                                                    <p>The amount equivalent to your order value will be deducted from your Online Checkout Finance credit limit. Whenever you repay the monthly installments, the amount equivalent to the principal value in your installments will be replenished in the Online Checkout Finance credit limit. But your regulatory limit may not increase (Point 8). </p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																		<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingTwelve">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Can I convert a transaction I already made using another payment method to Online Checkout Finance?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
                                                                            <div class="panel-body p-4">
                                                                                    <p>You will not be able to change the payment method of an order, once it is successfully placed. </p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	
																		<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingThirteen">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Can I change my EMI plan?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
                                                                            <div class="panel-body p-4">
                                                                                    <p>We are sorry to inform you that you will not be able to change your EMI plans once the order is placed.</p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	
																		<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFourteen">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Will I get reward points for using Online Checkout Finance?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFourteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourteen">
                                                                            <div class="panel-body p-4">
                                                                                    <p>No. You will not accrue any reward points for making bookings using Online Checkout Finance. However, if you repay using your debit card, you can accrue reward points on the repayments in your debit card, if applicable by your bank. </p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																		<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingFifteen">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Why am I being contacted by Capital Float claiming that I have taken a loan from them for online purchases?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseFifteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen">
                                                                            <div class="panel-body p-4">
                                                                                    <p>Several e-commerce companies have partnered with Capital Float, a Government of India recognized Non-banking financial corporation to provide Online Checkout Finance facility to our customers. Since our e-commerce partner doesn't hold the license to give credit to customers, Capital Float has provided credit to you via the e-commerce partner. Hence, you are receiving notifications from Capital Float.  </p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingSixteen">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSixteen" aria-expanded="false" aria-controls="collapseSixteen">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">My payment failed while placing an order, will I get an option to retry payment?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseSixteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSixteen">
                                                                            <div class="panel-body p-4">
                                                                                    <p>In case the payment failed on your order, you will get an option to retry the payment using a different payment method than Online Checkout Finance. For instructions on how to retry payment, please visit this <a href="https://www.amazon.in/gp/help/customer/display.html/?ie=UTF8&nodeId=202054720">help page.</a></p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
<!-- Cancellation starts here -->
		<h4 class="f-subtext">
			Cancellations
																	</h4>															
																	<div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingThirteen">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse17" aria-expanded="false" aria-controls="collapse17">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3">Can I cancel the purchase made using Online Checkout Finance?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapse17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
                                                                            <div class="panel-body p-4">
                                                                                    <p>Yes. Just like any other booking on partner website, you can cancel or request cancellation any time before availing the service.</p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingThirteen">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse18" aria-expanded="false" aria-controls="collapse18">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3"> In case of cancellation, how will the refund be processed</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapse18" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
                                                                            <div class="panel-body p-4">
                                                                                    <p>In case you have not paid even a single installment for the product, then on cancelling we will credit your EMI limit with the total order value.
<br>
If you've already made a payment towards EMI repayments, we will credit the paid amount back to your bank account / debit card, along with reinstating your Online Checkout Finance limit as mentioned above. This amount should be reflected in your bank account within 3-5 business days. We may reach out to you via Email /SMS /Call in case further details are required to process the refund.</p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	  <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="heading18">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse19" aria-expanded="false" aria-controls="collapse19">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                <span class="pl-3"> Do you charge any cancellation fee specifically for Online Checkout Finance?</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapse19" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
                                                                            <div class="panel-body p-4">
                                                                                    <p>There is no Online Checkout Finance specific cancellation fee in case you cancel the booking using this limit. However, the standard cancellation fees may still apply.</p>																																						
                                                                            </div>
                                                                        </div>
                                                                    </div>
																	
																	<!-- Cancellation ends here -->															
<!--  Online Checkout Finance Repayments starts -->
<h4 class="f-subtext">
	Online Checkout Finance Repayments
																	</h4>

      <div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse20" aria-expanded="false" aria-controls="collapse20">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">Do I have to make multiple payments in case I have placed more than one orders using Online Checkout Finance?</span>
                </a>
            </h4>
        </div>
        <div id="collapse20" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>We will club monthly installments across all your bookings with Online Checkout Finance into one total EMI amount. This amount will be due on the due date and will be deducted from your bank account/debit card basis the auto repayment setup you would have done.</p>																																						
            </div>
        </div>
    </div>

      <div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse21" aria-expanded="false" aria-controls="collapse21">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">When is my Online Checkout Finance repayment due?</span>
                </a>
            </h4>
        </div>
        <div id="collapse21" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>The first EMI for your booking will be due on the 5th day of the month next to the subsequent one.
                    <br>
						<br>For example: If you've made a transaction using Online Checkout Finance in April, the first EMI will be due on 5th of June (month next to the subsequent one). All the subsequent EMIs will be due on 5th of the subsequent months after June.</p>																																						
            </div>
        </div>
    </div>
									  	
    <div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse22" aria-expanded="false" aria-controls="collapse22">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">How does the Online Checkout Finance repayment work?</span>
                </a>
            </h4>
        </div>
        <div id="collapse22" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>Online Checkout Finance repayment can happen in two ways - (i) Auto repayment: which is a one-time setup during registration (ii) Manual repayments (link of which you will get with notifications on due dates)
                    <br><br>For auto repayment setup, you will have to link your debit card/bank account of selected banks by performing one verification transaction of upto Rs 5. This amount will be refunded within 3-5 business days to your bank account. Once this is set, the due amount will be automatically deducted from your linked bank account/debit card, thus making repayments hassle-free.</p>																																						
            </div>
        </div>
    </div>
	
    <div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse23" aria-expanded="false" aria-controls="collapse23">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">Why is Capital Float being shown in my bank statement or during auto-repayment setup?</span>
                </a>
            </h4>
        </div>
        <div id="collapse23" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>We have partnered with Capital Float to provide Online Checkout Finance credit to our customers. The repayment towards your EMI purchases are credited to Capital Float accounts.</p>																																						
            </div>
        </div>
    </div>
     <div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse24" aria-expanded="false" aria-controls="collapse24">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">What happens in case my auto-repayment setup fails during booking?</span>
                </a>
            </h4>
        </div>
        <div id="collapse24" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>Your transaction will be declined. You will have to retry the payment from Your Orders page for that particular order</p>																																						
            </div>
        </div>
    </div>

 <div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse25" aria-expanded="false" aria-controls="collapse25">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">Can I repay my partial outstanding amount?</span>
                </a>
            </h4>
        </div>
        <div id="collapse25" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>Currently, the capability to partly repay the outstanding dues is not available on the platform. However, you can pay total outstanding amount from the Online Checkout Finance dashboard without any additional charges. You will just have to pay the interest amount accrued till that day of the month.

</p>																																						
            </div>
        </div>
    </div>

<div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse26" aria-expanded="false" aria-controls="collapse26">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3"> What happens if my debit card/bank doesn't have sufficient funds to complete the payment?</span>
                </a>
            </h4>
        </div>
        <div id="collapse26" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>In such a case, we will notify you through email and SMS about transaction rejection from bank and details on how to manually complete the payment to avoid any late payment fees/penalties. Please note that your Online Checkout Finance account can be blocked after few days post the due date if the EMI payment is not received and will only be unblocked once the due payments are complete. If you complete the payments within 30 days of the due date, your Online Checkout Finance account will be unblocked immediately but if your payment comes after 30 days from due date, your account status will change post internal re-assessment.</p>																																						
            </div>
        </div>
    </div>

<div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse27" aria-expanded="false" aria-controls="collapse27">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3"> Will I incur any late payment fees/penalties for delayed payments?</span>
                </a>
            </h4>
        </div>
        <div id="collapse27" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>If you miss your payments on due date, following late payment penalties will apply:</p>																																						
            </div>
        </div>
    </div>

<div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse25" aria-expanded="false" aria-controls="collapse25">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">Can I change the auto repayment settings after registering for Online Checkout Finance?</span>
                </a>
            </h4>
        </div>
        <div id="collapse25" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>No, once the auto-repayment is setup, you will not be able to change the bank or repayment mode from partner App. This feature will be available on the App shortly. However, you can call or write to our lending partner's (Capital Float's) customer care center to get the auto repayment settings changed through an offline process. Contact details of lending partner - Call: 1800 419 0999 Email: myloan@capitalfloat.com</p>																																						
            </div>
        </div>
    </div>

<div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse29" aria-expanded="false" aria-controls="collapse29">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">Will I get any reminder notification for EMI payments?</span>
                </a>
            </h4>
        </div>
        <div id="collapse29" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>Yes, you will be notified about upcoming payments via SMS and/or Email 3-4 days before the due date so that you can keep the bank account well-funded for successful repayments.</p>																																						
            </div>
        </div>
    </div>



<div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse30" aria-expanded="false" aria-controls="collapse30">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3"> Can I change my repayment date?</span>
                </a>
            </h4>
        </div>
        <div id="collapse30" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>No.</p>																																						
            </div>
        </div>
    </div>

<div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse31" aria-expanded="false" aria-controls="collapse31">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">What happens if I don't pay my EMI on time?</span>
                </a>
            </h4>
        </div>
        <div id="collapse31" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>It's very important to pay your EMI on time in order to avoid late payment charges, maintain a good credit rating, avoid Online Checkout Finance account suspension, avoid actions on your account and avoid any legal action.<br><br>
                    Please maintain adequate balance in your account around the repayment dates. Not paying your EMI on time could result in your credit bureau (CIBIL) score being affected.</p>																																						
            </div>
        </div>
    </div>
    <div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse32" aria-expanded="false" aria-controls="collapse32">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">What should I do in case my bank account got debited by an amount that is more than my monthly installment or I've manually made a repayment, but still my money got deducted.</span>
                </a>
            </h4>
        </div>
        <div id="collapse32" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>In all such cases, the excess funds deducted will be credited back to the respective bank account from 'Capital Float (our lending partner)' in 3-5 business days.</p>																																						
            </div>
        </div>
    </div>
  											
																	
							<!--  Online Checkout Finance Repayments ends -->										
																	
																	
														
<!--     /*Online Checkout Finance Account Details start*/ -->
<h4 class="f-subtext">
	Online Checkout Finance Account Details
																	</h4>

    <div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse33" aria-expanded="false" aria-controls="collapse33">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">Why is my Online Checkout Finance account blocked?</span>
                </a>
            </h4>
        </div>
        <div id="collapse33" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>Your Online Checkout Finance account could be blocked due to multiple reasons:
                    <ul>
                   <li>Your KYC has expired</li>
                    <li>You have overdue payments or your repayment was post 30 days past due date</li>
                    <li>Due to internal partner website policies</li>
                    <li>Due to our partner's internal policies	</li>																																				
            </ul>
				</p>
            </div>
        </div>
    </div>

  <div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse34" aria-expanded="false" aria-controls="collapse34">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">What can I do to unblock the account?</span>
                </a>
            </h4>
        </div>
        <div id="collapse34" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>If your account is blocked due to payments not paid on time, please complete the pending payments. If the repayment is overdue for less than 30 days then your account will be unblocked automatically once we receive the payment confirmation. If the payment is made after 30 days of due date, we will reassess your profile and take the decision on reinstatement. If it is blocked due to expiry of KYC, we will provide details of the process to complete Full KYC and extend validity. If its blocked due to partner's policies, you have to wait for profile re-evaluation (which happens on periodic basis). We will notify you about the reinstatement of your account.</p>																																						
            </div>
        </div>
    </div>
      <div class="panel panel-default mb-3">
        <div class="panel-heading" role="tab" id="heading18">
            <h4 class="panel-title mb-0">
                <a class="collapsed faq-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse35" aria-expanded="false" aria-controls="collapse35">
                    <i class="far fa-plus-square"></i>
                    <!-- <i class="far fa-minus-square"></i> -->
                <span class="pl-3">How can I cancel my Online Checkout Finance account?</span>
                </a>
            </h4>
        </div>
        <div id="collapse35" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
            <div class="panel-body p-4">
                    <p>In order to cancel your Online Checkout Finance account, you'll have to ensure that all the outstanding dues are cleared. Only if there are no outstanding dues, we will be able to process account cancellation requests. If all the dues are clear, please call our customer service team to request account cancellation.</p>																																						
            </div>
        </div>
    </div>
    
<!--     /*Online Checkout Finance Account Details end*/				 -->
																	
																	
                                                            
                                                                </div><!-- panel-group -->
                                                                
                                                                
                                                            </div><!-- container -->
                                                            
                                                        
        
        
                                                        </div>


                                            </div>
                                            <div class="tab-pane fade" id="v-pills-cl-cs" role="tabpanel" aria-labelledby="v-pills-cl-cs-tab">

                                                    <div class="fc-slate-90 w-100" id="myTable1">
                                                            <div class="demo">
        
                                            
                                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            
                                                                    <div class="panel panel-default mb-3">
                                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                                            <h4 class="panel-title mb-0">
                                                                                <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                                    <i class="far fa-plus-square"></i>
                                                                                    <!-- <i class="far fa-minus-square"></i> -->
                                                                                    <span class="pl-3">Online Checkout Finance Customer Service</span>
                                                                                </a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                            <div class="panel-body p-4">
                                                                                   For any queries related to Online Checkout Finance, you can reach out to our partner customer care or Capital Float customer service - Contact details: Call at 1860 419 0999, or Email at <a mailto="myloan@capitalfloat.com">myloan@capitalfloat.com</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            
                                                                   
                                                                </div><!-- panel-group -->
                                                                
                                                                
                                                            </div><!-- container -->
                                                            
                                                        
        
        
                                                        </div>

                                                        
                                            </div>
                                          </div>
                                          
                            </div>

                        <!------------------------------------------------------2nd Tab Ends here ----------------------------------------->









                          </div>

            </div>
            
           
        </div>
    </div>
</section>
             
       



      </main>
<?php get_footer(); ?>