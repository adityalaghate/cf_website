<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<?php if ( '' !== get_the_post_thumbnail() && is_single() ) : ?>
<section class="banner-single banner-all" style="background-image:url(<?php echo get_the_post_thumbnail_url();?>)">
	<div class="container h-100">
		<div class="row h-100">
			<div class="d-flex align-items-center h-100 w-100">

			<?php
			if ( is_single() ) {
			the_title( '<h1 class="col-md-8 lh-43 pt-5 fc-white text-uppercase fw-600 entry-title">', '</h1>' );
		} 
		?>
<?php endif; ?>
			</div>
				</div>
			</div>
	
</section>
<section class="container">
	<div class="row">
		<div class="col-md-10 pt-5">
			
		
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
		
		if ( is_front_page() && is_home() ) {
			the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		} else {
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		}
		echo '<div class="entry-meta pb-3">';
				
					twentyseventeen_posted_on();
	
		//echo "<div><span class='category'>";
		//the_category(" ");
		//echo "</span></div>";
		echo '</div><!-- .entry-meta -->';
		?>
	<div class="entry-content">
		<?php
		/* translators: %s: Name of current post */
		the_content( sprintf(
			__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
			get_the_title()
		) );

		wp_link_pages( array(
			'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
</div>
		<div class="container">
<div class="row similar pb-5">
	<div class="col-12">
	<h3 class="py-3">
		More Related Posts
	</h3>
	<div class="entry-content row">
		<?php
		$related_args = array(
			'post_type' => $post->post_type,
			'posts_per_page' => 3,
			'orderby' => 'rand',
			'post__not_in' => array( get_the_ID() ),
		);
		$related = new WP_Query( $related_args );

		if( $related->have_posts() ) :
		?>
		
				<?php while( $related->have_posts() ): $related->the_post(); ?>
					<div class="col-md-4">
						<div class="card">
							<img class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" />
							<div class="card-body">
								<h5 class="card-title light-blue fs-16">
									<a href="<?php the_permalink(); ?>">
										<?php the_title(); ?>
									</a>  
								</h5>
								<div class="card-text font-sourcesanspro truncate"><?php the_content(); ?></div>

								<p><!--<span>by <?php the_author(); ?></span> | --><span>Oct 24, 2018 </span></p>
								<!--<div class="post-category">
									<?php the_category(' '); ?>
								</div>
								-->
							</div>
						</div>
		</div>
				<?php endwhile; ?>
			
		<?php
		endif;
		wp_reset_postdata();
		?>
	</div>
</div>
		</div>
</div>