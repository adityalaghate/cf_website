      </main>

       <!-- -----   Footer --------------->

    <footer class="footer pt-5 pb-3 pt-md-5 pb-md-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <h6 class="mb-3">Business Loans</h6>
                        <ul class="list-unstyled small">
                            <li><a href="/term-finance">Term Finance</a></li>
                            <li><a href="/loans-against-card-swipes">Loans Against Card Swipes</a></li>
                            <!--<li><a href="/doctor-loans">Doctor Loans</a></li>-->
                            <li><a href="/school-finance">School Finance</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <h6 class="mb-3">Consumer Finance</h6>
                        <ul class="list-unstyled small">
                            <li><a href="/online-checkout-finance">Online Checkout Finance</a></li>
                            <li><a href="/merchant-checkout-finance">Merchant Checkout Finance</a></li>
                            <li><a href="/walnut">Walnut</a></li>
                        </ul>
                        <h6 class="mt-4 mb-3">Company</h6>
                        <ul class="list-unstyled small">
                            <li><a href="/about-us">About Us</a></li>
							<li><a href="/team">Team</a></li>
                            <li><a href="/media-awards">Media &amp; Awards</a></li>
							<li><a href="/contact-us">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <h6 class="mb-3">Contact Us</h6>
                        <ul class="list-unstyled small">
                            <li><a href="/faq">FAQs</a></li>
                            <li><a href="/privacy-policy">Privacy Policy</a></li>
                            <li><a href="/regulatory">Regulatory</a></li>
                            <li><a href="/terms-conditions">Terms &amp; Conditions</a></li>
                        </ul>
                        <h6 class="mt-4 mb-3">Partner with Us</h6>
                        <ul class="list-unstyled small">
                            <li><a href="/dsa">For DSA</a></li>
							<li><a href="/enterprise">For Enterprise</a></li>
                            <li><a href="/consumer-loans">For Consumer Loans</a></li>
                            <li><a href="/co-lending">For Co-lending</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3">
                            <h6 class="mb-3">Contact Details</h6>
                            <ul class="list-unstyled small">
<!--                                 <li class="fc-footlink">See All Locations</li> -->
                                <li class="fc-footlink fs-16 fw-600"><a href="tel:18604190999"><i class="fa fa-phone d-sm-none"></i></a> 1860 419 0999</li>
                                <li class="fs-16 fw-600"><a href="mailto:info@capitalfloat.com" class="fc-footlink">info@capitalfloat.com</a></li>
                                <li class="fc-footlink pt-4"><em>Follow us on</em></li>
                                 <li class="footer-social">
                                    <a href="https://twitter.com/CapitalFloat" class="cf-tw" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png" alt="Twitter" /></a>
                                    <a href="https://www.facebook.com/capitalfloat" class="cf-fb" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png" alt="Facebook" /></a>
                                    <a href="https://www.linkedin.com/company/capital-float" class="cf-li" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/linkedin.png" alt="LinkedIn"/></a>
                                </li>
                            </ul>
                      
    
                    </div>
    
                </div>
		</div>
		<div class="container border-top">
				<div class="row pt-4">
					<div class="col-md-11">
						<p class="fc-footlink fs-13">
						&copy; Capital Float. Capital Float is the trade name of CapFloat Financial Services Private Limited (Formerly known as Zen Lefin Private Limited), a non-banking finance company (NBFC) registered with the RBI</p>
					</div>
					<div class="col-md-1">
						<img src="/wp-content/uploads/2019/10/secure.png" />
					</div>
				</div>
            </div>
		
        </footer>
    
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<script  src="<?php echo get_template_directory_uri(); ?>/js/banner-carousel.js"></script>
<script  src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<script  src="<?php echo get_template_directory_uri(); ?>/js/slick.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/range-slider.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/base.js"></script>
    <?php wp_footer();?>

</body>    

</html>