<?php /* Template Name: media-awards */ ?>
<?php get_header(); ?>
        
<main>
	<section class="w-100 media py-md-5 py-3">
    <div class="container">
        <div class="row">   
<div class="col-12">
<h5 class="fw-600 pl-1 pb-3 fc-slate2 text-uppercase"><?php the_field('section1_heading'); ?></h5>

</div>

                <div class="rio-promos col-12">
					
					<?php if( have_rows('section1_card') ): ?>

	

	<?php while( have_rows('section1_card') ): the_row();?>
					
                        <div class="card" style="width: 18rem;">
							<a href="<?php the_sub_field('sec1_card_link'); ?>">


                        
                                <div class="position-relative img-div"> 
                 <!--   <img src="<?php the_sub_field('sec1_card_logo'); ?>" alt="logo-tc" class="pb-2 logo"/>-->

                                <img src="<?php the_sub_field('sec1_card_image'); ?>" alt="our-products-image" class="card-img-top">
                                </div>
                                <div class="card-body pl-0 pr-5 pt-3">    
                                    <h6 class="card-title fc-slate lh-24"><?php the_sub_field('sec1_card_title'); ?></h6>                                    
                                
                                </div>
								</a>
                            </div>
                           
					<?php endwhile; ?>

<?php endif; ?>
			</div>

        </div>

    </div>

</section>

<section class="w-100 media-awards py-md-5 py-3  text-center position-relative">
    <div class="container">
        <div class="row">   
			<img src="<?php echo get_template_directory_uri(); ?>/images/pattern-right.png" alt="patern-image" class=" position-absolute d-none d-md-block ma-a">
            <div class="col-12 pb-md-4 pb-2">
                <h5 class="text-left text-md-center fw-600 pl-1 pb-3 text-uppercase fc-slate"><?php the_field('section2_heading'); ?></h5>          
                </div>
            <div class="rio-promos-awards col-12">
				
				<?php if( have_rows('section2_card') ): ?>

	<?php while( have_rows('section2_card') ): the_row();?>

				
                <div class="card text-center p-md-4 py-4 py-md-0">                    
                    <div class="position-relative img-div pt-2"> 
                        <img src="<?php the_sub_field('sec2_card_image'); ?>" alt="our-products-image" class="card-img-media-awards mx-auto">
                    </div>
                    <div class="card-body pt-3 pb-0">    
                      <!--  <h5 class="card-title fc-slate fw-600 mb-1 "><?php the_sub_field('sec2_card_year'); ?></h5> -->   
                        <span class="fc-gray"><?php the_sub_field('sec2_card_awardedby'); ?></span>
                       <!-- <p class="fc-blue text-uppercase ls-1 fw-600 pt-1 m-0"><?php the_sub_field('sec2_card_award_name'); ?></p>-->
                    </div>
            </div>
				
				<?php endwhile; ?>


<?php endif; ?>
       
       
                                
            </div>
        </div>
    </div>
</section>


<section class="w-100 py-md-5 py-3 presskit">
    <div class="container">
        <div class="row">   
            <div class="col-12">
            </div>                    
            <div class="col-md-5 col-12 offset-md-1 mb-3 mb-md-0">
                    <h5 class="fw-600 pb-3 fc-slate2 text-uppercase"><?php the_field('section3_heading'); ?></h5>

                <p class="fc-slate2"><?php the_field('section3_desc'); ?></p>
                <div class="button-wrap pt-2 col-12 pl-0">
                        <button class="btn-apply fs-12 px-3 py-2"><?php the_field('section3_btn_text'); ?></button>
                    </div> 
            </div>
            <div class="col-md-4 col-12 offset-md-1">
                    <img src="<?php the_field('section3_image'); ?>" alt="who-we-are" class="w-100"/>
                </div>
        </div>
    </div>
</section>


<section class="w-100 py-md-5 py-3 archives fc-slate">
    <div class="container">
        <div class="row">  
            <div class="col-12">
                    <h5 class="fw-600 pb-3 fc-slate2 text-uppercase"><?php the_field('section4_heading'); ?></h5>
                    

            </div> 
            <div class="col-12">
                <div class="cont">
                    <div class="container p-0">
                        <div class="row">   
							
							
							<?php if( have_rows('section4_card') ): ?>
							<?php while( have_rows('section4_card') ): the_row();?>

							
                            <div class="single-item col-md-3 col-12 mb-3">
                                <div class="card h-100">
                                    <img class="card-img-top" src="<?php the_sub_field('sec4_card_image'); ?>" alt="Card image cap">
                                    <div class="card-body p-0">
                                        <div class="card-body-inner px-3 pb-2">
                                           <!-- <img src="<?php the_sub_field('sec4_card_logo'); ?>" />
                                            <span class="float-right fs-12"><?php the_sub_field('sec4_card_year'); ?></span>-->
                                            
                                        </div>
                                        <p class="card-text px-2 py-2 pb-0 fs-13"><a href="<?php the_sub_field('archive_link'); ?>"><?php the_sub_field('sec4_card_desc'); ?></a></p>
                                    </div>
                                </div>
                            </div>
							
							<?php endwhile; ?>
						<?php endif; ?>
                                       
                       
                           
                    
                        </div>
                    </div>
                </div>
            </div>                 
        </div>
    </div>
</section>
    

      </main>
<?php get_footer(); ?>