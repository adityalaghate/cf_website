<?php /* Template Name: terms-conditions */ ?>
<?php get_header(); ?>
        
<main>
	
	
<section class="w-100  py-3 privacy-policy">
    <div class="container">
        <div class="row">
            <div class="col-md-10 p-md-5 fc-slate-90">
                <!-- <h5 class="fc-slate fw-600 text-uppercase pb-3 text-center">frequently asked questions</h5> -->
                <div class="demo p-md-3">
					<?php if( have_rows('section1_cards') ): ?>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">  
						<?php $i=1; while ( have_rows('section1_cards') ) : the_row(); ?>
                        <div class="panel panel-default mb-3">
                            <div class="panel-heading" role="tab" id="headingOne-<?php echo $i; ?>">
                                <h4 class="panel-title mb-0">
                                    <a role="button" data-toggle="collapse" class="faq-collapse collapsed" data-parent="#accordion" href="#collapseOne-<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="far fa-plus-square"></i>
                                        <!-- <i class="far fa-minus-square"></i> -->
                                        <span class="pl-3">	<?php the_sub_field('sec1_card_heading'); ?></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne-<?php echo $i; ?>" class="panel-collapse collapse <?php if ($i==1) { echo ''; } ?>" role="tabpanel" aria-labelledby="headingOne-<?php echo $i; ?>">
                                <div class="panel-body p-4">
                                        <p>	<?php the_sub_field('sec1_card_desc'); ?></p>

                                                
                                </div>
                            </div>
                        </div>                
                                    <?php $i++; endwhile; ?>                  
                    </div><!-- panel-group -->   
					<?php endif; ?>
                </div><!-- container -->
            </div>            
        </div>
    </div>
</section>
        




     



      </main>
<?php get_footer(); ?>